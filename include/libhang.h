/*
 * Copyright (C) 2014-2016 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 3 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef LIBHANG_H
#define LIBHANG_H 1

#define lh_mark_used(var) ((void)(var))

#include <libhang/types.h>

#include <libhang/bitwise.h>
#include <libhang/ll.h>
#include <libhang/ll_s.h>
#include <libhang/llit.h>
#include <libhang/llit_s.h>
#include <libhang/images.h>
#include <libhang/fb2d.h>
#include <libhang/skybox.h>
#include <libhang/core.h>
#include <libhang/driver.h>
#include <libhang/keys.h>

#include <libhang/perlinnoise.h>
#include <libhang/opensimplexnoise.h>

#endif /* LIBHANG_H */
