/*
 * Copyright (C) 2014-2016 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 3 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef LIBHANG_OBJ_FILE_H
#define LIBHANG_OBJ_FILE_H 1

#include "core.h"

#ifdef __cplusplus
extern "C" {
#endif

	extern struct lh_obj_class **lhCreateObjClassesFromOBJ(const char *data);
	extern struct lh_obj_class **lhCreateObjClassesFromOBJFile(const char *path);
	extern struct lh_obj_class **lhCreateObjClassesFromFile(const char *path);

#ifdef __cplusplus
}
#endif

#endif /* LIBHANG_OBJ_FILE_H */
