/*
 * Copyright (C) 2014-2016 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 3 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef LIBHANG_KEYS_H
#define LIBHANG_KEYS_H 1

/* ASCII keycodes */

/* Controls */

#define LHK_back_space  0x0008
#define LHK_tab         0x0009
#define LHK_enter       0x000A
#define LHK_escape      0x001B
#define LHK_del         0x00FF

/* Printable */

#define LHK_space       0x0020

#define LHK_asterisk    0x002A
#define LHK_plus        0x002B
#define LHK_minus       0x002D
#define LHK_slash       0x002F

#define LHK_0           0x0030
#define LHK_1           0x0031
#define LHK_2           0x0032
#define LHK_3           0x0033
#define LHK_4           0x0034
#define LHK_5           0x0035
#define LHK_6           0x0036
#define LHK_7           0x0037
#define LHK_8           0x0038
#define LHK_9           0x0039

#define LHK_a           0x0061
#define LHK_b           0x0062
#define LHK_c           0x0063
#define LHK_d           0x0064
#define LHK_e           0x0065
#define LHK_f           0x0066
#define LHK_g           0x0067
#define LHK_h           0x0068
#define LHK_i           0x0069
#define LHK_j           0x006A
#define LHK_k           0x006B
#define LHK_l           0x006C
#define LHK_m           0x006D
#define LHK_n           0x006E
#define LHK_o           0x006F
#define LHK_p           0x0070
#define LHK_q           0x0071
#define LHK_r           0x0072
#define LHK_s           0x0073
#define LHK_t           0x0074
#define LHK_u           0x0075
#define LHK_v           0x0076
#define LHK_w           0x0077
#define LHK_x           0x0078
#define LHK_y           0x0079
#define LHK_z           0x007A

/* Other keycodes */

#define LHK_shift       0x0100
#define LHK_ctrl        0x0101
#define LHK_alt         0x0102
#define LHK_pause       0x0103
#define LHK_caps_lock   0x0104
#define LHK_scroll_lock 0x0105
#define LHK_sys_req     0x0106

#define LHK_f1          0x0120
#define LHK_f2          0x0121
#define LHK_f3          0x0122
#define LHK_f4          0x0123
#define LHK_f5          0x0124
#define LHK_f6          0x0125
#define LHK_f7          0x0126
#define LHK_f8          0x0127
#define LHK_f9          0x0128
#define LHK_f10         0x0129
#define LHK_f11         0x012A
#define LHK_f12         0x012B
#define LHK_left        0x012C
#define LHK_right       0x012D
#define LHK_up          0x012E
#define LHK_down        0x012F

#define LHK_MAX         0x012F

#endif /* LIBHANG_KEYS_H */
