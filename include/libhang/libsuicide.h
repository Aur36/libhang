/*
 * Copyright (C) 2014-2016 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 3 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef LIBHANG_LIBSUICIDE_H
#define LIBHANG_LIBSUICIDE_H 1

#ifndef INSIDE_LIBHANG_TYPES_H
#  error "Never use <libhang/libsuicide.h> directly, please include <libhang/types.h> instead."
#else

#include <float.h>
#include <math.h>

/* For Windows... */
#undef near
#undef far

#define LH_PI 3.1415926535897932384626433832795

#define LH_DISTANCES(n, f) ((lh_distances_t){ (n), (f) })

#define LH_VEC2(x, y) ((lh_vec2_t){ {(x), (y)} })
#define LH_VEC3(x, y, z) ((lh_vec3_t){{ (x), (y), (z)} })
#define LH_VEC4(x, y, z, w) ((lh_vec4_t){ {(x), (y), (z), (w)} })

#define LH_IVEC2(x, y) ((lh_ivec2_t){ {(x), (y)} })
#define LH_IVEC3(x, y, z) ((lh_ivec3_t){{ (x), (y), (z)} })
#define LH_IVEC4(x, y, z, w) ((lh_ivec4_t){ {(x), (y), (z), (w)} })

#define LH_UVEC2(x, y) ((lh_uvec2_t){ {(x), (y)} })
#define LH_UVEC3(x, y, z) ((lh_uvec3_t){{ (x), (y), (z)} })
#define LH_UVEC4(x, y, z, w) ((lh_uvec4_t){ {(x), (y), (z), (w)} })

#define LH_QUAT(x, y, z, w) ((lh_quat_t){ {(x), (y), (z), (w)} })

#define LH_MAT2(x0, y0, x1, y1) ((lh_mat2_t){ { (x0), (y0), (x1), (y1) } })
#define LH_MAT3(x0, y0, z0, x1, y1, z1, x2, y2, z2) ((lh_mat3_t){ { (x0), (y0), (z0), (x1), (y1), (z1), (x2), (y2), (z2) } })
#define LH_MAT4(x0, y0, z0, w0, x1, y1, z1, w1, x2, y2, z2, w2, x3, y3, z3, w3) ((lh_mat4_t){ { (x0), (y0), (z0), (w0), (x1), (y1), (z1), (w1), (x2), (y2), (z2), (w2), (x3), (y3), (z3), (w3) } })

#define LH_RGB(r, g, b) ((lh_rgb_t){ {(r), (g), (b)} })
#define LH_RGB888(r, g, b) ((lh_rgb888_t){ {(r), (g), (b)} })
#define LH_RGBA(r, g, b, a) ((lh_rgba_t){ {(r), (g), (b), (a)} })
#define LH_RGBA8888(r, g, b, a) ((lh_rgba8888_t){ {(r), (g), (b), (a)} })

#define LH_LINE(p, q) ((lh_line_t){ (p), (q) })
#define LH_PLANE(p, q) ((lh_plane_t){ (p), (q) })
#define LH_SPACE(p, q) ((lh_space_t){ (p), (q) })
#define LH_CUBE(p, q, s) ((lh_cube_t){ ((lh_space_t){ (p), (q) }), (s) })

typedef struct {
	float near, far;
} lh_distances_t;

typedef union {
	float a[2];
	struct {
		float x, y;
	} c;
} lh_vec2_t;
typedef union {
	float a[3];
	struct {
		float x, y, z;
	} c;
} lh_vec3_t;
typedef union {
	float a[4];
	struct {
		float x, y, z, w;
	} c;
} lh_vec4_t;

typedef union {
	int a[2];
	struct {
		int x, y;
	} c;
} lh_ivec2_t;
typedef union {
	int a[3];
	struct {
		int x, y, z;
	} c;
} lh_ivec3_t;
typedef union {
	int a[4];
	struct {
		int x, y, z, w;
	} c;
} lh_ivec4_t;

typedef union {
	LHuint a[2];
	struct {
		LHuint x, y;
	} c;
} lh_uvec2_t;
typedef union {
	LHuint a[3];
	struct {
		LHuint x, y, z;
	} c;
} lh_uvec3_t;
typedef union {
	LHuint a[4];
	struct {
		LHuint x, y, z, w;
	} c;
} lh_uvec4_t;

typedef lh_vec4_t lh_quat_t;

typedef union {
	float a[2][2];
	float la[4];
	struct {
		float x0, y0;
		float x1, y1;
	} c;
} lh_mat2_t;
typedef union {
	float a[3][3];
	float la[9];
	struct {
		float x0, y0, z0;
		float x1, y1, z1;
		float x2, y2, z2;
	} c;
} lh_mat3_t;
typedef union {
	float a[4][4];
	float la[16];
	struct {
		float x0, y0, z0, w0;
		float x1, y1, z1, w1;
		float x2, y2, z2, w2;
		float x3, y3, z3, w3;
	} c;
} lh_mat4_t;

typedef union {
	float a[3];
	struct {
		float r, g, b;
	} comp;
} lh_rgb_t;
typedef union {
	LHubyte a[3];
	struct {
		LHubyte r, g, b;
	} comp;
} lh_rgb888_t;
typedef union {
	float a[4];
	struct {
		float r, g, b, a;
	} comp;
} lh_rgba_t;
typedef union {
	LHubyte a[4];
	struct {
		LHubyte r, g, b, a;
	} comp;
} lh_rgba8888_t;

typedef struct {
	lh_vec3_t p;
	lh_quat_t q;
} lh_pq_t;
typedef lh_pq_t lh_line_t;
typedef lh_pq_t lh_plane_t;
typedef lh_pq_t lh_space_t;

typedef struct {
	lh_pq_t pq;
	float s;
} lh_pqs_t;
typedef lh_pqs_t lh_cube_t;

static inline void lh_quat_conjug(lh_quat_t *r, lh_quat_t *a);
static inline void lh_quat_hamilton(lh_quat_t *r, lh_quat_t *a, lh_quat_t *b);

static inline void lh_mat4_invert(lh_mat4_t *r, lh_mat4_t *a);
static inline void lh_mat4_transpose(lh_mat4_t *r, lh_mat4_t *a);

static inline float minf(float a, float b) {
	return a < b ? a : b;
}

static inline double mind(double a, double b) {
	return a < b ? a : b;
}

static inline int mini(int a, int b) {
	return a < b ? a : b;
}

static inline float maxf(float a, float b) {
	return a > b ? a : b;
}

static inline double maxd(double a, double b) {
	return a > b ? a : b;
}

static inline int maxi(int a, int b) {
	return a > b ? a : b;
}

static inline float clampf(float d, float min, float max) {
	const float t = d < min ? min : d;
	return t > max ? max : t;
}

static inline double clampd(double d, double min, double max) {
	const double t = d < min ? min : d;
	return t > max ? max : t;
}

static inline int clampi(int d, int min, int max) {
	const int t = d < min ? min : d;
	return t > max ? max : t;
}

static inline LHbool betweenf(float a, float b, float c) {
	if(b < c)
		return b <= a && a <= c;
	else
		return c <= a && a <= b;
}

static inline LHbool betweend(double a, double b, double c) {
	if(b < c)
		return b <= a && a <= c;
	else
		return c <= a && a <= b;
}

static inline LHbool betweeni(int a, int b, int c) {
	if(b < c)
		return b <= a && a <= c;
	else
		return c <= a && a <= b;
}

static inline void lh_distances_dup(lh_distances_t *r, lh_distances_t *a) {
	r->near = a->near;
	r->far  = a->far;
}

static inline void lh_distances_zero(lh_distances_t *r) {
	r->near = 0.f;
	r->far = 0.f;
}

static inline void lh_vec2_dup(lh_vec2_t *r, lh_vec2_t *a) {
	float *R = r->a;
	float *A = a->a;

	R[0] = A[0];
	R[1] = A[1];
}

static inline void lh_vec2_zero(lh_vec2_t *r) {
	float *R = r->a;

	R[0] = 0.f;
	R[1] = 0.f;
}

static inline void lh_vec2_neg(lh_vec2_t *r, lh_vec2_t *a) {
	float *R = r->a;
	float *A = a->a;

	R[0] = -A[0];
	R[1] = -A[1];
}

static inline void lh_vec2_add(lh_vec2_t *r, lh_vec2_t *a, lh_vec2_t *b) {
	float *R = r->a;
	float *A = a->a;
	float *B = b->a;

	R[0] = A[0] + B[0];
	R[1] = A[1] + B[1];
}

static inline void lh_vec2_sub(lh_vec2_t *r, lh_vec2_t *a, lh_vec2_t *b) {
	float *R = r->a;
	float *A = a->a;
	float *B = b->a;

	R[0] = A[0] - B[0];
	R[1] = A[1] - B[1];
}

static inline void lh_vec2_scale(lh_vec2_t *r, lh_vec2_t *a, float s) {
	float *R = r->a;
	float *A = a->a;

	R[0] = A[0] * s;
	R[1] = A[1] * s;
}

static inline float lh_vec2_mul_inner(lh_vec2_t *a, lh_vec2_t *b) {
	float *A = a->a;
	float *B = b->a;

	return A[0]*B[0] + A[1]*B[1];
}

static inline float lh_vec2_len(lh_vec2_t *a) {
	return sqrt(lh_vec2_mul_inner(a, a));
}

static inline void lh_vec2_norm(lh_vec2_t *r, lh_vec2_t *a) {
	const float s = 1.f / lh_vec2_len(a);
	lh_vec2_scale(r, a, s);
}

static inline void lh_vec2_rotate(lh_vec2_t *r, lh_vec2_t *a, float t) {
	float *R = r->a;
	float *A = a->a;
	const float ct = cos(t);
	const float st = sin(t);

	const float x = A[0] * ct - A[1] * st;
	R[1] = A[0] * st + A[1] * ct;
	R[0] = x;
}

static inline void lh_vec3_dup(lh_vec3_t *r, lh_vec3_t *a) {
	float *R = r->a;
	float *A = a->a;

	R[0] = A[0];
	R[1] = A[1];
	R[2] = A[2];
}

static inline void lh_vec3_zero(lh_vec3_t *r) {
	float *R = r->a;

	R[0] = 0.f;
	R[1] = 0.f;
	R[2] = 0.f;
}

static inline void lh_vec3_neg(lh_vec3_t *r, lh_vec3_t *a) {
	float *R = r->a;
	float *A = a->a;

	R[0] = -A[0];
	R[1] = -A[1];
	R[2] = -A[2];
}

static inline void lh_vec3_add(lh_vec3_t *r, lh_vec3_t *a, lh_vec3_t *b) {
	float *R = r->a;
	float *A = a->a;
	float *B = b->a;

	R[0] = A[0] + B[0];
	R[1] = A[1] + B[1];
	R[2] = A[2] + B[2];
}

static inline void lh_vec3_sub(lh_vec3_t *r, lh_vec3_t *a, lh_vec3_t *b) {
	float *R = r->a;
	float *A = a->a;
	float *B = b->a;

	R[0] = A[0] - B[0];
	R[1] = A[1] - B[1];
	R[2] = A[2] - B[2];
}

static inline void lh_vec3_scale(lh_vec3_t *r, lh_vec3_t *a, float s) {
	float *R = r->a;
	float *A = a->a;

	R[0] = A[0] * s;
	R[1] = A[1] * s;
	R[2] = A[2] * s;
}

static inline float lh_vec3_mul_inner(lh_vec3_t *a, lh_vec3_t *b) {
	float *A = a->a;
	float *B = b->a;

	return A[0]*B[0] + A[1]*B[1] + A[2]*B[2];
}

static inline void lh_vec3_mul_cross(lh_vec3_t *r, lh_vec3_t *a, lh_vec3_t *b) {
	float *R = r->a;
	float *A = a->a;
	float *B = b->a;

	R[0] = A[1]*B[2] - A[2]*B[1];
	R[1] = A[2]*B[0] - A[0]*B[2];
	R[2] = A[0]*B[1] - A[1]*B[0];
}

static inline float lh_vec3_len(lh_vec3_t *a) {
	return sqrt(lh_vec3_mul_inner(a, a));
}

static inline void lh_vec3_norm(lh_vec3_t *r, lh_vec3_t *a) {
	const float s = 1.f / lh_vec3_len(a);
	lh_vec3_scale(r, a, s);
}

static inline void lh_vec3_rotateX(lh_vec3_t *r, lh_vec3_t *a, float t) {
	float *R = r->a;
	float *A = a->a;
	const float ct = cos(t);
	const float st = sin(t);

	const float y = A[1] * ct - A[2] * st;
	R[2] = A[1] * st + A[2] * ct;
	R[1] = y;
}

static inline void lh_vec3_rotateY(lh_vec3_t *r, lh_vec3_t *a, float t) {
	float *R = r->a;
	float *A = a->a;
	const float ct = cos(t);
	const float st = sin(t);

	const float z = A[0] * st + A[2] * ct;
	R[0] = A[0] * ct - A[2] * st;
	R[2] = z;
}

static inline void lh_vec3_rotateZ(lh_vec3_t *r, lh_vec3_t *a, float t) {
	float *R = r->a;
	float *A = a->a;
	const float ct = cos(t);
	const float st = sin(t);

	const float x = A[0] * ct - A[1] * st;
	R[1] = A[0] * st + A[1] * ct;
	R[0] = x;
}

static inline void lh_vec3_rotate_quat(lh_vec3_t *r, lh_vec3_t *a, lh_quat_t *q) {
	float *R = r->a;
	float *A = a->a;
	lh_quat_t rq, q_;

	lh_quat_conjug(&q_, q);
	lh_quat_hamilton(&rq, q, &LH_QUAT(A[0], A[1], A[2], 0.f));
	lh_quat_hamilton(&rq, &rq, &q_);

	R[0] = rq.c.x;
	R[1] = rq.c.y;
	R[2] = rq.c.z;
}

static inline void lh_vec3_ux_rotate_quat(lh_vec3_t *r, lh_quat_t *q) {
	lh_vec3_rotate_quat(r, &LH_VEC3(1.f, 0.f, 0.f), q);
}

static inline void lh_vec3_uy_rotate_quat(lh_vec3_t *r, lh_quat_t *q) {
	lh_vec3_rotate_quat(r, &LH_VEC3(0.f, 1.f, 0.f), q);
}

static inline void lh_vec3_uz_rotate_quat(lh_vec3_t *r, lh_quat_t *q) {
	lh_vec3_rotate_quat(r, &LH_VEC3(0.f, 0.f, 1.f), q);
}

static inline void lh_vec3_euler_from_quat(lh_vec3_t *r, lh_quat_t *q) {
	float *R = r->a;
	float *Q = q->a;
	const float q11 = Q[1] * Q[1];
	const float sat = Q[1] * Q[3] - Q[0] * Q[2];

	if(fabs(sat - 0.5f) < FLT_EPSILON) {
		R[0] = 2.f*atan2f(Q[0], Q[3]);
		R[1] = LH_PI / 2.f;
		R[2] = 0.f;
	} else if(fabs(sat + 0.5f) < FLT_EPSILON) {
		R[0] = 2.f*atan2f(Q[0], Q[3]);
		R[1] = -LH_PI / 2.f;
		R[2] = 0.f;
	} else {
		R[0] = atan2f(2.f * (Q[3] * Q[0] + Q[2] * Q[1]), 1.f - 2.f * (Q[0] * Q[0] + q11));
		R[1] = asinf(2.f * sat);
		R[2] = atan2f(2.f * (Q[3] * Q[2] + Q[0] * Q[1]), 1.f - 2.f * (Q[2] * Q[2] + q11));
	}
}

static inline void lh_vec4_dup(lh_vec4_t *r, lh_vec4_t *a) {
	float *R = r->a;
	float *A = a->a;

	R[0] = A[0];
	R[1] = A[1];
	R[2] = A[2];
	R[3] = A[3];
}

static inline void lh_vec4_zero(lh_vec4_t *r) {
	float *R = r->a;

	R[0] = 0.f;
	R[1] = 0.f;
	R[2] = 0.f;
	R[3] = 0.f;
}

static inline void lh_vec4_neg(lh_vec4_t *r, lh_vec4_t *a) {
	float *R = r->a;
	float *A = a->a;

	R[0] = -A[0];
	R[1] = -A[1];
	R[2] = -A[2];
	R[3] = -A[3];
}

static inline void lh_vec4_add(lh_vec4_t *r, lh_vec4_t *a, lh_vec4_t *b) {
	float *R = r->a;
	float *A = a->a;
	float *B = b->a;

	R[0] = A[0] + B[0];
	R[1] = A[1] + B[1];
	R[2] = A[2] + B[2];
	R[3] = A[3] + B[3];
}

static inline void lh_vec4_sub(lh_vec4_t *r, lh_vec4_t *a, lh_vec4_t *b) {
	float *R = r->a;
	float *A = a->a;
	float *B = b->a;

	R[0] = A[0] - B[0];
	R[1] = A[1] - B[1];
	R[2] = A[2] - B[2];
	R[3] = A[3] - B[3];
}

static inline void lh_vec4_scale(lh_vec4_t *r, lh_vec4_t *a, float s) {
	float *R = r->a;
	float *A = a->a;

	R[0] = A[0] * s;
	R[1] = A[1] * s;
	R[2] = A[2] * s;
	R[3] = A[3] * s;
}

static inline float lh_vec4_mul_inner(lh_vec4_t *a, lh_vec4_t *b) {
	float *A = a->a;
	float *B = b->a;

	return A[0]*B[0] + A[1]*B[1] + A[2]*B[2] + A[3]*B[3];
}

static inline float lh_vec4_len(lh_vec4_t *a) {
	return sqrt(lh_vec4_mul_inner(a, a));
}

static inline void lh_vec4_norm(lh_vec4_t *r, lh_vec4_t *a) {
	const float s = 1.f / lh_vec4_len(a);
	lh_vec4_scale(r, a, s);
}

static inline void lh_ivec2_dup(lh_ivec2_t *r, lh_ivec2_t *a) {
	int *R = r->a;
	int *A = a->a;

	R[0] = A[0];
	R[1] = A[1];
}

static inline void lh_ivec3_dup(lh_ivec3_t *r, lh_ivec3_t *a) {
	int *R = r->a;
	int *A = a->a;

	R[0] = A[0];
	R[1] = A[1];
	R[2] = A[2];
}

static inline void lh_ivec4_dup(lh_ivec4_t *r, lh_ivec4_t *a) {
	int *R = r->a;
	int *A = a->a;

	R[0] = A[0];
	R[1] = A[1];
	R[2] = A[2];
	R[3] = A[3];
}

static inline void lh_uvec2_dup(lh_uvec2_t *r, lh_uvec2_t *a) {
	LHuint *R = r->a;
	LHuint *A = a->a;

	R[0] = A[0];
	R[1] = A[1];
}

static inline void lh_uvec3_dup(lh_uvec3_t *r, lh_uvec3_t *a) {
	LHuint *R = r->a;
	LHuint *A = a->a;

	R[0] = A[0];
	R[1] = A[1];
	R[2] = A[2];
}

static inline void lh_uvec4_dup(lh_uvec4_t *r, lh_uvec4_t *a) {
	LHuint *R = r->a;
	LHuint *A = a->a;

	R[0] = A[0];
	R[1] = A[1];
	R[2] = A[2];
	R[3] = A[3];
}

static inline void lh_quat_from_vec3_euler(lh_quat_t *r, lh_vec3_t *a) {
	float *R = r->a;
	float *A = a->a;
	const float c0 = cosf(A[0] / 2.f);
	const float c1 = cosf(A[1] / 2.f);
	const float c2 = cosf(A[2] / 2.f);
	const float s0 = sinf(A[0] / 2.f);
	const float s1 = sinf(A[1] / 2.f);
	const float s2 = sinf(A[2] / 2.f);

	R[0] = s0 * c1 * c2 - c0 * s1 * s2;
	R[1] = c0 * s1 * c2 + s0 * c1 * s2;
	R[2] = c0 * c1 * s2 - s0 * s1 * c2;
	R[3] = c0 * c1 * c2 + s0 * s1 * s2;
}

static inline void lh_quat_from_two_vec3(lh_quat_t *r, lh_vec3_t *a, lh_vec3_t *b) {
	float *R = r->a;
	lh_vec3_t w;
	float rlen;

	lh_vec3_mul_cross(&w, a, b);

	R[0] = w.c.x;
	R[1] = w.c.y;
	R[2] = w.c.z;
	R[3] = lh_vec3_mul_inner(a, b);

	rlen = lh_vec4_len(r);
	R[3] += rlen;

	lh_vec4_norm(r, r);
}

static inline void lh_quat_conjug(lh_quat_t *r, lh_quat_t *a) {
	float *R = r->a;
	float *A = a->a;

	R[0] = -A[0];
	R[1] = -A[1];
	R[2] = -A[2];
	R[3] = A[3];
}

static inline void lh_quat_hamilton(lh_quat_t *r, lh_quat_t *a, lh_quat_t *b) {
	float *R = r->a;
	float *A = a->a;
	float *B = b->a;
	float as = A[3], bs = B[3];
	lh_vec3_t av = LH_VEC3(A[0], A[1], A[2]);
	lh_vec3_t bv = LH_VEC3(B[0], B[1], B[2]);
	lh_vec3_t ab_cross, r3;

	R[3] = as*bs - lh_vec3_mul_inner(&av, &bv);

	lh_vec3_mul_cross(&ab_cross, &av, &bv);
	lh_vec3_scale(&av, &av, bs);
	lh_vec3_scale(&bv, &bv, as);
	lh_vec3_add(&r3, &av, &bv);
	lh_vec3_add(&r3, &r3, &ab_cross);

	R[0] = r3.c.x;
	R[1] = r3.c.y;
	R[2] = r3.c.z;
}

static inline void lh_mat2_dup(lh_mat2_t *r, lh_mat2_t *a) {
	float (*R)[2] = r->a;
	float (*A)[2] = a->a;

	R[0][0] = A[0][0];
	R[0][1] = A[0][1];

	R[1][0] = A[1][0];
	R[1][1] = A[1][1];
}

static inline void lh_mat2_zero(lh_mat2_t *r) {
	float (*R)[2] = r->a;

	R[0][0] = 0.f;
	R[0][1] = 0.f;

	R[1][0] = 0.f;
	R[1][1] = 0.f;
}

static inline void lh_mat2_identity(lh_mat2_t *r) {
	float (*R)[2] = r->a;

	R[0][0] = 1.f;
	R[0][1] = 0.f;

	R[1][0] = 0.f;
	R[1][1] = 1.f;
}

static inline void lh_mat2_invert(lh_mat2_t *r, lh_mat2_t *a) {
	float (*R)[2] = r->a;
	float (*A)[2] = a->a;
	float idet;

	idet = 1.0f / (A[0][0] * A[1][1] - A[0][1] * A[1][0]);

	R[0][0] =  A[1][1] * idet;
	R[0][1] = -A[0][1] * idet;
	R[1][0] = -A[1][0] * idet;
	R[1][1] =  A[0][0] * idet;
}

static inline void lh_mat3_dup(lh_mat3_t *r, lh_mat3_t *a) {
	float (*R)[3] = r->a;
	float (*A)[3] = a->a;

	R[0][0] = A[0][0];
	R[0][1] = A[0][1];
	R[0][2] = A[0][2];

	R[1][0] = A[1][0];
	R[1][1] = A[1][1];
	R[1][2] = A[1][2];

	R[2][0] = A[2][0];
	R[2][1] = A[2][1];
	R[2][2] = A[2][2];
}

static inline void lh_mat3_zero(lh_mat3_t *r) {
	float (*R)[3] = r->a;

	R[0][0] = 0.f;
	R[0][1] = 0.f;
	R[0][2] = 0.f;

	R[1][0] = 0.f;
	R[1][1] = 0.f;
	R[1][2] = 0.f;

	R[2][0] = 0.f;
	R[2][1] = 0.f;
	R[2][2] = 0.f;
}

static inline void lh_mat3_from_mat4(lh_mat3_t *r, lh_mat4_t *a) {
	float (*R)[3] = r->a;
	float (*A)[4] = a->a;

	R[0][0] = A[0][0];
	R[0][1] = A[0][1];
	R[0][2] = A[0][2];

	R[1][0] = A[1][0];
	R[1][1] = A[1][1];
	R[1][2] = A[1][2];

	R[2][0] = A[2][0];
	R[2][1] = A[2][1];
	R[2][2] = A[2][2];
}

static inline void lh_mat3_normal_from_mat4(lh_mat3_t *r, lh_mat4_t *a) {
	lh_mat4_t m, n;
	lh_mat4_transpose(&m, a);
	lh_mat4_invert(&n, &m);
	lh_mat3_from_mat4(r, &n);
}

static inline void lh_mat3_identity(lh_mat3_t *r) {
	float (*R)[3] = r->a;

	R[0][0] = 1.f;
	R[0][1] = 0.f;
	R[0][2] = 0.f;

	R[1][0] = 0.f;
	R[1][1] = 1.f;
	R[1][2] = 0.f;

	R[2][0] = 0.f;
	R[2][1] = 0.f;
	R[2][2] = 1.f;
}

static inline void lh_mat3_mul(lh_mat3_t *r, lh_mat3_t *a, lh_mat3_t *b) {
	float (*R)[3] = r->a;
	float (*A)[3] = a->a;
	float (*B)[3] = b->a;

	R[0][0] = B[0][0]*A[0][0] + B[0][1]*A[1][0] + B[0][2]*A[2][0];
	R[0][1] = B[0][0]*A[0][1] + B[0][1]*A[1][1] + B[0][2]*A[2][1];
	R[0][2] = B[0][0]*A[0][2] + B[0][1]*A[1][2] + B[0][2]*A[2][2];

	R[1][0] = B[1][0]*A[0][0] + B[1][1]*A[1][0] + B[1][2]*A[2][0];
	R[1][1] = B[1][0]*A[0][1] + B[1][1]*A[1][1] + B[1][2]*A[2][1];
	R[1][2] = B[1][0]*A[0][2] + B[1][1]*A[1][2] + B[1][2]*A[2][2];

	R[2][0] = B[2][0]*A[0][0] + B[2][1]*A[1][0] + B[2][2]*A[2][0];
	R[2][1] = B[2][0]*A[0][1] + B[2][1]*A[1][1] + B[2][2]*A[2][1];
	R[2][2] = B[2][0]*A[0][2] + B[2][1]*A[1][2] + B[2][2]*A[2][2];
}

/* Rotate around the 3 orthogonal axis. */
static inline void lh_mat3_rotXYZ(lh_mat3_t *r, lh_mat3_t *a, lh_vec3_t *angle) {
	float *T = angle->a;
	lh_mat3_t m;
	float (*M)[3] = m.a;

	float cx = cos(T[0]);
	float cy = cos(T[1]);
	float cz = cos(T[2]);
	float sx = sin(T[0]);
	float sy = sin(T[1]);
	float sz = sin(T[2]);

	M[0][0] = cy*cz;
	M[0][1] = cy*sz;
	M[0][2] = -sy;

	M[1][0] = sx*sy*cz - cx*sz;
	M[1][1] = sx*sy*sz + cx*cz;
	M[1][2] = sx*cy;

	M[2][0] = cx*sy*cz + sx*sz;
	M[2][1] = cx*sy*sz - sx*cz;
	M[2][2] = cx*cy;

	lh_mat3_mul(r, a, &m);
}

/* Rotate around a vector */
static inline void lh_mat3_rotate(lh_mat3_t *r, lh_mat3_t *a, lh_vec3_t *v, float angle) {
	lh_mat3_t m;
	float (*M)[3] = m.a;
	float c, ic, s;
	lh_vec3_t u;
	float x, y, z;

	c = cos(angle);
	ic = 1.f - c;
	s = sin(angle);
	lh_vec3_norm(&u, v);
	x = u.c.x;
	y = u.c.y;
	z = u.c.z;

	M[0][0] = x*x*ic + c;
	M[0][1] = x*y*ic + z*s;
	M[0][2] = x*z*ic - y*s;

	M[1][0] = y*x*ic - z*s;
	M[1][1] = y*y*ic + c;
	M[1][2] = y*z*ic + x*s;

	M[2][0] = z*x*ic + y*s;
	M[2][1] = z*y*ic - x*s;
	M[2][2] = z*z*ic + c;

	lh_mat3_mul(r, a, &m);
}

static inline void lh_mat4_dup(lh_mat4_t *r, lh_mat4_t *a) {
	float (*R)[4] = r->a;
	float (*A)[4] = a->a;

	R[0][0] = A[0][0];
	R[0][1] = A[0][1];
	R[0][2] = A[0][2];
	R[0][3] = A[0][3];

	R[1][0] = A[1][0];
	R[1][1] = A[1][1];
	R[1][2] = A[1][2];
	R[1][3] = A[1][3];

	R[2][0] = A[2][0];
	R[2][1] = A[2][1];
	R[2][2] = A[2][2];
	R[2][3] = A[2][3];

	R[3][0] = A[3][0];
	R[3][1] = A[3][1];
	R[3][2] = A[3][2];
	R[3][3] = A[3][3];
}

static inline void lh_mat4_zero(lh_mat4_t *r) {
	float (*R)[4] = r->a;

	R[0][0] = 0.f;
	R[0][1] = 0.f;
	R[0][2] = 0.f;
	R[0][3] = 0.f;

	R[1][0] = 0.f;
	R[1][1] = 0.f;
	R[1][2] = 0.f;
	R[1][3] = 0.f;

	R[2][0] = 0.f;
	R[2][1] = 0.f;
	R[2][2] = 0.f;
	R[2][3] = 0.f;

	R[3][0] = 0.f;
	R[3][1] = 0.f;
	R[3][2] = 0.f;
	R[3][3] = 0.f;
}

static inline void lh_mat4_dup_from_mat3(lh_mat4_t *r, lh_mat3_t *a) {
	float (*R)[4] = r->a;
	float (*A)[3] = a->a;

	R[0][0] = A[0][0];
	R[0][1] = A[0][1];
	R[0][2] = A[0][2];

	R[1][0] = A[1][0];
	R[1][1] = A[1][1];
	R[1][2] = A[1][2];

	R[2][0] = A[2][0];
	R[2][1] = A[2][1];
	R[2][2] = A[2][2];
}

static inline void lh_mat4_from_mat3(lh_mat4_t *r, lh_mat3_t *a) {
	float (*R)[4] = r->a;

	lh_mat4_dup_from_mat3(r, a);
	R[0][3] = 0.f;
	R[1][3] = 0.f;
	R[2][3] = 0.f;
	R[3][0] = 0.f;
	R[3][1] = 0.f;
	R[3][2] = 0.f;
	R[3][3] = 0.f;
}

static inline void lh_mat4_identity_from_mat3(lh_mat4_t *r, lh_mat3_t *a) {
	float (*R)[4] = r->a;

	lh_mat4_from_mat3(r, a);
	R[3][3] = 1.f;
}

static inline void lh_mat4_identity(lh_mat4_t *r) {
	float (*R)[4] = r->a;

	R[0][0] = 1.f;
	R[0][1] = 0.f;
	R[0][2] = 0.f;
	R[0][3] = 0.f;

	R[1][0] = 0.f;
	R[1][1] = 1.f;
	R[1][2] = 0.f;
	R[1][3] = 0.f;

	R[2][0] = 0.f;
	R[2][1] = 0.f;
	R[2][2] = 1.f;
	R[2][3] = 0.f;

	R[3][0] = 0.f;
	R[3][1] = 0.f;
	R[3][2] = 0.f;
	R[3][3] = 1.f;
}

static inline void lh_mat4_mul(lh_mat4_t *r, lh_mat4_t *a, lh_mat4_t *b) {
	float (*R)[4] = r->a;
	float (*A)[4] = a->a;
	float (*B)[4] = b->a;

	R[0][0] = B[0][0]*A[0][0] + B[0][1]*A[1][0] + B[0][2]*A[2][0] + B[0][3]*A[3][0];
	R[0][1] = B[0][0]*A[0][1] + B[0][1]*A[1][1] + B[0][2]*A[2][1] + B[0][3]*A[3][1];
	R[0][2] = B[0][0]*A[0][2] + B[0][1]*A[1][2] + B[0][2]*A[2][2] + B[0][3]*A[3][2];
	R[0][3] = B[0][0]*A[0][3] + B[0][1]*A[1][3] + B[0][2]*A[2][3] + B[0][3]*A[3][3];

	R[1][0] = B[1][0]*A[0][0] + B[1][1]*A[1][0] + B[1][2]*A[2][0] + B[1][3]*A[3][0];
	R[1][1] = B[1][0]*A[0][1] + B[1][1]*A[1][1] + B[1][2]*A[2][1] + B[1][3]*A[3][1];
	R[1][2] = B[1][0]*A[0][2] + B[1][1]*A[1][2] + B[1][2]*A[2][2] + B[1][3]*A[3][2];
	R[1][3] = B[1][0]*A[0][3] + B[1][1]*A[1][3] + B[1][2]*A[2][3] + B[1][3]*A[3][3];

	R[2][0] = B[2][0]*A[0][0] + B[2][1]*A[1][0] + B[2][2]*A[2][0] + B[2][3]*A[3][0];
	R[2][1] = B[2][0]*A[0][1] + B[2][1]*A[1][1] + B[2][2]*A[2][1] + B[2][3]*A[3][1];
	R[2][2] = B[2][0]*A[0][2] + B[2][1]*A[1][2] + B[2][2]*A[2][2] + B[2][3]*A[3][2];
	R[2][3] = B[2][0]*A[0][3] + B[2][1]*A[1][3] + B[2][2]*A[2][3] + B[2][3]*A[3][3];

	R[3][0] = B[3][0]*A[0][0] + B[3][1]*A[1][0] + B[3][2]*A[2][0] + B[3][3]*A[3][0];
	R[3][1] = B[3][0]*A[0][1] + B[3][1]*A[1][1] + B[3][2]*A[2][1] + B[3][3]*A[3][1];
	R[3][2] = B[3][0]*A[0][2] + B[3][1]*A[1][2] + B[3][2]*A[2][2] + B[3][3]*A[3][2];
	R[3][3] = B[3][0]*A[0][3] + B[3][1]*A[1][3] + B[3][2]*A[2][3] + B[3][3]*A[3][3];
}

static inline void lh_mat4_translate(lh_mat4_t *r, lh_vec3_t *v) {
	float (*R)[4] = r->a;

	lh_mat4_identity(r);
	lh_vec3_dup((lh_vec3_t *)R[3], v);
}

static inline void lh_mat4_translate_in_place(lh_mat4_t *r, lh_vec3_t *v) {
	lh_mat4_t a, b;
	lh_mat4_dup(&a, r);
	lh_mat4_translate(&b, v);
	lh_mat4_mul(r, &a, &b);
}

/* Rotate around the 3 orthogonal axis. */
static inline void lh_mat4_rotXYZ(lh_mat4_t *r, lh_mat4_t *a, lh_vec3_t *angle) {
	float *T = angle->a;
	lh_mat4_t m;
	float (*M)[4] = m.a;

	float cx = cos(T[0]);
	float cy = cos(T[1]);
	float cz = cos(T[2]);
	float sx = sin(T[0]);
	float sy = sin(T[1]);
	float sz = sin(T[2]);

	M[0][0] = cy*cz;
	M[0][1] = cy*sz;
	M[0][2] = -sy;
	M[0][3] = 0.f;

	M[1][0] = sx*sy*cz - cx*sz;
	M[1][1] = sx*sy*sz + cx*cz;
	M[1][2] = sx*cy;
	M[1][3] = 0.f;

	M[2][0] = cx*sy*cz + sx*sz;
	M[2][1] = cx*sy*sz - sx*cz;
	M[2][2] = cx*cy;
	M[2][3] = 0.f;

	M[3][0] = 0.f;
	M[3][1] = 0.f;
	M[3][2] = 0.f;
	M[3][3] = 1.f;

	lh_mat4_mul(r, a, &m);
}

/* Rotate around a vector */
static inline void lh_mat4_rotate(lh_mat4_t *r, lh_mat4_t *a, lh_vec3_t *v, float angle) {
	lh_mat4_t m;
	float (*M)[4] = m.a;
	float c, ic, s;
	lh_vec3_t u;
	float x, y, z;

	c = cos(angle);
	ic = 1.f - c;
	s = sin(angle);
	lh_vec3_norm(&u, v);
	x = u.c.x;
	y = u.c.y;
	z = u.c.z;

	M[0][0] = x*x*ic + c;
	M[0][1] = x*y*ic + z*s;
	M[0][2] = x*z*ic - y*s;
	M[0][3] = 0.f;

	M[1][0] = y*x*ic - z*s;
	M[1][1] = y*y*ic + c;
	M[1][2] = y*z*ic + x*s;
	M[1][3] = 0.f;

	M[2][0] = z*x*ic + y*s;
	M[2][1] = z*y*ic - x*s;
	M[2][2] = z*z*ic + c;
	M[2][3] = 0.f;

	M[3][0] = 0.f;
	M[3][1] = 0.f;
	M[3][2] = 0.f;
	M[3][3] = 1.f;

	lh_mat4_mul(r, a, &m);
}

static inline void lh_mat4_invert(lh_mat4_t *r, lh_mat4_t *a) {
	float (*R)[4] = r->a;
	float (*A)[4] = a->a;
	float s[6];
	float c[6];
	float idet;

	s[0] = A[0][0]*A[1][1] - A[1][0]*A[0][1];
	s[1] = A[0][0]*A[1][2] - A[1][0]*A[0][2];
	s[2] = A[0][0]*A[1][3] - A[1][0]*A[0][3];
	s[3] = A[0][1]*A[1][2] - A[1][1]*A[0][2];
	s[4] = A[0][1]*A[1][3] - A[1][1]*A[0][3];
	s[5] = A[0][2]*A[1][3] - A[1][2]*A[0][3];

	c[0] = A[2][0]*A[3][1] - A[3][0]*A[2][1];
	c[1] = A[2][0]*A[3][2] - A[3][0]*A[2][2];
	c[2] = A[2][0]*A[3][3] - A[3][0]*A[2][3];
	c[3] = A[2][1]*A[3][2] - A[3][1]*A[2][2];
	c[4] = A[2][1]*A[3][3] - A[3][1]*A[2][3];
	c[5] = A[2][2]*A[3][3] - A[3][2]*A[2][3];

	idet = 1.0f/(s[0]*c[5]-s[1]*c[4]+s[2]*c[3]+s[3]*c[2]-s[4]*c[1]+s[5]*c[0]);

	R[0][0] = ( A[1][1] * c[5] - A[1][2] * c[4] + A[1][3] * c[3]) * idet;
	R[0][1] = (-A[0][1] * c[5] + A[0][2] * c[4] - A[0][3] * c[3]) * idet;
	R[0][2] = ( A[3][1] * s[5] - A[3][2] * s[4] + A[3][3] * s[3]) * idet;
	R[0][3] = (-A[2][1] * s[5] + A[2][2] * s[4] - A[2][3] * s[3]) * idet;

	R[1][0] = (-A[1][0] * c[5] + A[1][2] * c[2] - A[1][3] * c[1]) * idet;
	R[1][1] = ( A[0][0] * c[5] - A[0][2] * c[2] + A[0][3] * c[1]) * idet;
	R[1][2] = (-A[3][0] * s[5] + A[3][2] * s[2] - A[3][3] * s[1]) * idet;
	R[1][3] = ( A[2][0] * s[5] - A[2][2] * s[2] + A[2][3] * s[1]) * idet;

	R[2][0] = ( A[1][0] * c[4] - A[1][1] * c[2] + A[1][3] * c[0]) * idet;
	R[2][1] = (-A[0][0] * c[4] + A[0][1] * c[2] - A[0][3] * c[0]) * idet;
	R[2][2] = ( A[3][0] * s[4] - A[3][1] * s[2] + A[3][3] * s[0]) * idet;
	R[2][3] = (-A[2][0] * s[4] + A[2][1] * s[2] - A[2][3] * s[0]) * idet;

	R[3][0] = (-A[1][0] * c[3] + A[1][1] * c[1] - A[1][2] * c[0]) * idet;
	R[3][1] = ( A[0][0] * c[3] - A[0][1] * c[1] + A[0][2] * c[0]) * idet;
	R[3][2] = (-A[3][0] * s[3] + A[3][1] * s[1] - A[3][2] * s[0]) * idet;
	R[3][3] = ( A[2][0] * s[3] - A[2][1] * s[1] + A[2][2] * s[0]) * idet;
}

static inline void lh_mat4_transpose(lh_mat4_t *r, lh_mat4_t *a) {
	float (*R)[4] = r->a;
	float (*A)[4] = a->a;

	R[0][0] = A[0][0];
	R[0][1] = A[1][0];
	R[0][2] = A[2][0];
	R[0][3] = A[3][0];

	R[1][0] = A[0][1];
	R[1][1] = A[1][1];
	R[1][2] = A[2][1];
	R[1][3] = A[3][1];

	R[2][0] = A[0][2];
	R[2][1] = A[1][2];
	R[2][2] = A[2][2];
	R[2][3] = A[3][2];

	R[3][0] = A[0][3];
	R[3][1] = A[1][3];
	R[3][2] = A[2][3];
	R[3][3] = A[3][3];
}

static inline void lh_mat4_perspective(lh_mat4_t *r, float fovy, float aspect, float near, float far) {
	float (*R)[4] = r->a;
	float f = 1.f / tan(fovy / 2.f);

	R[0][0] = f / aspect;
	R[0][1] = 0.f;
	R[0][2] = 0.f;
	R[0][3] = 0.f;

	R[1][0] = 0.f;
	R[1][1] = f;
	R[1][2] = 0.f;
	R[1][3] = 0.f;

	R[2][0] = 0.f;
	R[2][1] = 0.f;
	R[2][2] = (far + near) / (near - far);
	R[2][3] = -1.f;

	R[3][0] = 0.f;
	R[3][1] = 0.f;
	R[3][2] = 2.f * far * near / (near - far);
	R[3][3] = 0.f;
}

static inline void lh_mat4_model(lh_mat4_t *r, lh_vec3_t *pos, lh_vec3_t *angle) {
	lh_mat4_t m;

	lh_mat4_identity(&m);
	lh_mat4_translate_in_place(&m, pos);
	lh_mat4_rotXYZ(r, &m, angle);
}

static inline void lh_mat4_view(lh_mat4_t *r, lh_vec3_t *pos, lh_vec3_t *angle) {
	lh_mat4_t m;
	lh_vec3_t ipos, iangle;

	/* The camera is inverted relative to the scene. */
	lh_vec3_neg(&ipos, pos);
	lh_vec3_neg(&iangle, angle);

	lh_mat4_identity(&m);
	lh_mat4_rotXYZ(r, &m, &iangle);
	lh_mat4_translate_in_place(r, &ipos);
}

static inline void lh_rgb_dup(lh_rgb_t *r, lh_rgb_t *a) {
	float *R = r->a;
	float *A = a->a;

	R[0] = A[0];
	R[1] = A[1];
	R[2] = A[2];
}

static inline void lh_rgb888_dup(lh_rgb888_t *r, lh_rgb888_t *a) {
	LHubyte *R = r->a;
	LHubyte *A = a->a;

	R[0] = A[0];
	R[1] = A[1];
	R[2] = A[2];
}

static inline void lh_rgba_dup(lh_rgba_t *r, lh_rgba_t *a) {
	float *R = r->a;
	float *A = a->a;

	R[0] = A[0];
	R[1] = A[1];
	R[2] = A[2];
	R[3] = A[3];
}

static inline void lh_rgba8888_dup(lh_rgba8888_t *r, lh_rgba8888_t *a) {
	LHubyte *R = r->a;
	LHubyte *A = a->a;

	R[0] = A[0];
	R[1] = A[1];
	R[2] = A[2];
	R[3] = A[3];
}

static inline LHbool lh_point_intersects_point(lh_vec3_t *point_a, lh_vec3_t *point_b) {
	float *PA = point_a->a;
	float *PB = point_b->a;

	return PA[0] == PB[0] && PA[1] == PB[1] && PA[2] == PB[2];
}

static inline LHbool lh_point_intersects_line(lh_vec3_t *point, lh_line_t *line) {
	lh_vec3_t lp, line_v, cross;

	lh_vec3_ux_rotate_quat(&line_v, &line->q);
	lh_vec3_sub(&lp, point, &line->p);
	lh_vec3_mul_cross(&cross, &lp, &line_v);

	return fabs(cross.c.x) < FLT_EPSILON && fabs(cross.c.y) < FLT_EPSILON && fabs(cross.c.z) < FLT_EPSILON;
}

/*
 * Computes the distance along the line between its origin and its intersection
 * with the plane, in direction vector unit.
 */
static inline float lh_line_intersects_plane_dist_orig(lh_line_t *line, lh_plane_t *plane) {
	lh_vec3_t line_v, plane_v1, plane_v2, normal;
	float pa, pb, pc, pd;

	lh_vec3_ux_rotate_quat(&line_v, &line->q);
	lh_vec3_ux_rotate_quat(&plane_v1, &plane->q);
	lh_vec3_uy_rotate_quat(&plane_v2, &plane->q);

	lh_vec3_mul_cross(&normal, &plane_v1, &plane_v2);
	pa = normal.c.x;
	pb = normal.c.y;
	pc = normal.c.z;
	pd = -lh_vec3_mul_inner(&normal, &plane->p);
	return -(pa * line->p.c.x + pb * line->p.c.y + pc * line->p.c.z + pd) / (pa * line_v.c.x + pb * line_v.c.y + pc * line_v.c.z);
}

static inline void lh_line_intersects_plane(lh_line_t *line, lh_plane_t *plane, lh_vec3_t *inter_point) {
	lh_vec3_t line_v;
	const float t = lh_line_intersects_plane_dist_orig(line, plane);

	lh_vec3_ux_rotate_quat(&line_v, &line->q);
	inter_point->c.x = line->p.c.x + t * line_v.c.x;
	inter_point->c.y = line->p.c.y + t * line_v.c.y;
	inter_point->c.z = line->p.c.z + t * line_v.c.z;
}

static inline LHbool lh_line_intersects_cube(lh_line_t *line, lh_cube_t *cube, lh_vec3_t *inter_point) {
	lh_vec3_t cl, cl_new, lp_new, lv_new, near_top;
	lh_quat_t cq_conj, lq_new;
	lh_line_t line_new;
	float tx, ty, tz, tmax;

	lh_vec3_sub(&cl, &line->p, &cube->pq.p);

	lh_quat_conjug(&cq_conj, &cube->pq.q);
	lh_vec3_rotate_quat(&cl_new, &cl, &cq_conj);

	lh_vec3_add(&lp_new, &cube->pq.p, &cl_new);
	lh_vec4_add(&lq_new, &line->q, &cq_conj);

	line_new = LH_LINE(lp_new, lq_new);

	lh_vec3_ux_rotate_quat(&lv_new, &lq_new);
	lh_vec3_dup(&near_top, &cube->pq.p);
	if(cl.c.x >= 0.f)
		near_top.c.x += cube->s / 2.f;
	else
		near_top.c.x -= cube->s / 2.f;

	if(cl.c.y >= 0.f)
		near_top.c.y += cube->s / 2.f;
	else
		near_top.c.y -= cube->s / 2.f;

	if(cl.c.z >= 0.f)
		near_top.c.z += cube->s / 2.f;
	else
		near_top.c.z -= cube->s / 2.f;

	tx = lh_line_intersects_plane_dist_orig(&line_new, &LH_PLANE(near_top, LH_QUAT(0.f, 1.f, 0.f, 0.f)));
	ty = lh_line_intersects_plane_dist_orig(&line_new, &LH_PLANE(near_top, LH_QUAT(1.f, 0.f, 0.f, 0.f)));
	tz = lh_line_intersects_plane_dist_orig(&line_new, &LH_PLANE(near_top, LH_QUAT(0.f, 0.f, 0.f, 1.f)));

	tmax = maxf(maxf(tx, ty), tz);

	inter_point->c.x = tmax * lv_new.c.x + lp_new.c.x;
	inter_point->c.y = tmax * lv_new.c.y + lp_new.c.y;
	inter_point->c.z = tmax * lv_new.c.z + lp_new.c.z;

	if(inter_point->c.x < cube->pq.p.c.x - cube->s / 2.f || inter_point->c.x > cube->pq.p.c.x + cube->s / 2.f
		|| inter_point->c.y < cube->pq.p.c.y - cube->s / 2.f || inter_point->c.y > cube->pq.p.c.y + cube->s / 2.f
		|| inter_point->c.z < cube->pq.p.c.z - cube->s / 2.f || inter_point->c.z > cube->pq.p.c.z + cube->s / 2.f)
	{
		inter_point->c.x = INFINITY;
		inter_point->c.y = INFINITY;
		inter_point->c.z = INFINITY;
		return LH_FALSE;
	}
	return LH_TRUE;
}

#endif /* INSIDE_LIBHANG_TYPES_H */

#endif /* LIBHANG_LIBSUICIDE_H */
