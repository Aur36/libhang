/*
 * Copyright (C) 2014 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#define IMPLEMENT_API
#include <hx/CFFI.h>

#include <libhang/core.h>

#include "kinds.h"

DEFINE_KIND(hx_lh_window_t_ptr)
DEFINE_KIND(hx_lh_obj_class_t_ptr)
DEFINE_KIND(hx_lh_obj_t_ptr)

extern "C" {
	static value hx_lhCoreInit(value window) {
		lhCoreInit((lh_window_t *)val_get_handle(window, hx_lh_window_t_ptr));

		return alloc_null();
	}

	static value hx_lhGetRenderDistances(value window) {
		value distances;
		LHfloat min, max;
		lhGetRenderDistances((lh_window_t *)val_get_handle(window, hx_lh_window_t_ptr), &min, &max);

		distances = alloc_empty_object();
		alloc_field(distances, val_id("min"), alloc_float(min));
		alloc_field(distances, val_id("max"), alloc_float(max));

		return distances;
	}

	static value hx_lhSetRenderDistances(value window, value min, value max) {
		lhSetRenderDistances((lh_window_t *)val_get_handle(window, hx_lh_window_t_ptr), val_get_double(min), val_get_double(max));

		return alloc_null();
	}

	static value hx_lhGetBackColor(value window) {
		value dst_color;
		lh_rgba_t color;
		lhGetBackColor((lh_window_t *)val_get_handle(window, hx_lh_window_t_ptr), &color);

		dst_color = alloc_empty_object();
		alloc_field(dst_color, val_id("r"), alloc_float(color.r));
		alloc_field(dst_color, val_id("g"), alloc_float(color.g));
		alloc_field(dst_color, val_id("b"), alloc_float(color.b));
		alloc_field(dst_color, val_id("a"), alloc_float(color.a));

		return dst_color;
	}

	static value hx_lhSetBackColor(value window, value src_color) {
		lh_rgba_t color;

		color.r = val_field_numeric(src_color, val_id("r"));
		color.g = val_field_numeric(src_color, val_id("g"));
		color.b = val_field_numeric(src_color, val_id("b"));
		color.a = val_field_numeric(src_color, val_id("a"));
		lhSetBackColor((lh_window_t *)val_get_handle(window, hx_lh_window_t_ptr), &color);

		return src_color;
	}

	static value hx_lhGetAmbientLight(value window) {
		value dst_color;
		lh_rgb_t color;
		lhGetAmbientLight((lh_window_t *)val_get_handle(window, hx_lh_window_t_ptr), &color);

		dst_color = alloc_empty_object();
		alloc_field(dst_color, val_id("r"), alloc_float(color.r));
		alloc_field(dst_color, val_id("g"), alloc_float(color.g));
		alloc_field(dst_color, val_id("b"), alloc_float(color.b));

		return dst_color;
	}

	static value hx_lhSetAmbientLight(value window, value src_color) {
		lh_rgb_t color;

		color.r = val_field_numeric(src_color, val_id("r"));
		color.g = val_field_numeric(src_color, val_id("g"));
		color.b = val_field_numeric(src_color, val_id("b"));
		lhSetAmbientLight((lh_window_t *)val_get_handle(window, hx_lh_window_t_ptr), &color);

		return src_color;
	}

	static value hx_lhEnableLight(value window, value num, value enabled) {
		lhEnableLight((lh_window_t *)val_get_handle(window, hx_lh_window_t_ptr), val_get_int(num), val_get_bool(enabled) ? LH_TRUE : LH_FALSE);

		return alloc_null();
	}

	static value hx_lhSetLightParameters(value window, value num, value src_color, value src_direction) {
		lh_rgb_t color;
		lh_vec3_t direction;

		color.r = val_field_numeric(src_color, val_id("r"));
		color.g = val_field_numeric(src_color, val_id("g"));
		color.b = val_field_numeric(src_color, val_id("b"));

		direction.x = val_field_numeric(src_direction, val_id("x"));
		direction.y = val_field_numeric(src_direction, val_id("y"));
		direction.z = val_field_numeric(src_direction, val_id("z"));

		lhSetLightParameters((lh_window_t *)val_get_handle(window, hx_lh_window_t_ptr), val_get_int(num), &color, &direction);

		return alloc_null();
	}

	static value hx_lhGetCameraPos(value window) {
		value dst_pos;
		lh_vec3_t pos;
		lhGetCameraPos((lh_window_t *)val_get_handle(window, hx_lh_window_t_ptr), &pos);

		dst_pos = alloc_empty_object();
		alloc_field(dst_pos, val_id("x"), alloc_float(pos.x));
		alloc_field(dst_pos, val_id("y"), alloc_float(pos.y));
		alloc_field(dst_pos, val_id("z"), alloc_float(pos.z));

		return dst_pos;
	}

	static value hx_lhSetCameraPos(value window, value src_pos) {
		lh_vec3_t pos;

		pos.x = val_field_numeric(src_pos, val_id("x"));
		pos.y = val_field_numeric(src_pos, val_id("y"));
		pos.z = val_field_numeric(src_pos, val_id("z"));
		lhSetCameraPos((lh_window_t *)val_get_handle(window, hx_lh_window_t_ptr), &pos);

		return src_pos;
	}

	static value hx_lhGetCameraAngle(value window) {
		value dst_angle;
		lh_vec3_t angle;
		lhGetCameraAngle((lh_window_t *)val_get_handle(window, hx_lh_window_t_ptr), &angle);

		dst_angle = alloc_empty_object();
		alloc_field(dst_angle, val_id("x"), alloc_float(angle.x));
		alloc_field(dst_angle, val_id("y"), alloc_float(angle.y));
		alloc_field(dst_angle, val_id("z"), alloc_float(angle.z));

		return dst_angle;
	}

	static value hx_lhSetCameraAngle(value window, value src_angle) {
		lh_vec3_t angle;

		angle.x = val_field_numeric(src_angle, val_id("x"));
		angle.y = val_field_numeric(src_angle, val_id("y"));
		angle.z = val_field_numeric(src_angle, val_id("z"));
		lhSetCameraAngle((lh_window_t *)val_get_handle(window, hx_lh_window_t_ptr), &angle);

		return src_angle;
	}

	static value hx_lhEnableFog(value window, value enabled) {
		lhEnableFog((lh_window_t *)val_get_handle(window, hx_lh_window_t_ptr), val_get_bool(enabled) ? LH_TRUE : LH_FALSE);

		return alloc_null();
	}

	static value hx_lhSetFogParameters(value window, value min_distance, value max_distance, value src_color) {
		lh_rgb_t color;
		lh_vec3_t direction;

		color.r = val_field_numeric(src_color, val_id("r"));
		color.g = val_field_numeric(src_color, val_id("g"));
		color.b = val_field_numeric(src_color, val_id("b"));

		lhSetFogParameters((lh_window_t *)val_get_handle(window, hx_lh_window_t_ptr), val_get_double(min_distance), val_get_double(max_distance), &color);

		return alloc_null();
	}

	static value hx_lhLoadTextureFromImage(value window, value image) {
		return alloc_int(lhLoadTextureFromImage((lh_window_t *)val_get_handle(window, hx_lh_window_t_ptr), (lh_image_t *)val_get_handle(image, hx_lh_image_t_ptr)));
	}

	static value hx_lhLoadTextureRGB(value window, value data, value width, value height) {
		return alloc_int(lhLoadTextureRGB((lh_window_t *)val_get_handle(window, hx_lh_window_t_ptr), (lh_rgb888_t *)buffer_data(val_to_buffer(data)), val_get_int(width), val_get_int(height)));
	}

	static value hx_lhLoadTextureRGBA(value window, value data, value width, value height) {
		return alloc_int(lhLoadTextureRGBA((lh_window_t *)val_get_handle(window, hx_lh_window_t_ptr), (lh_rgba8888_t *)buffer_data(val_to_buffer(data)), val_get_int(width), val_get_int(height)));
	}

	static value hx_lhRegObjClass(value window, value obj_class) {
		lh_obj_class_t pobj_class;
		value values;

		pobj_class.name = (LHchar *)val_get_string(val_field(obj_class, val_id("name")));

		value shader_program = val_field(obj_class, val_id("shader_program"));

		values = val_field(val_field(shader_program, val_id("vertex_src")), val_id("src"));

		pobj_class.shader_program.vertex_src.nb_lines = val_array_size(values);
		pobj_class.shader_program.vertex_src.src = (LHchar **)malloc(sizeof(*pobj_class.shader_program.vertex_src.src) * pobj_class.shader_program.vertex_src.nb_lines);

		for(int i=0; i<pobj_class.shader_program.vertex_src.nb_lines; i++)
			pobj_class.shader_program.vertex_src.src[i] = (LHchar *)val_get_string(val_array_i(values, i));

		values = val_field(val_field(shader_program, val_id("fragment_src")), val_id("src"));

		pobj_class.shader_program.fragment_src.nb_lines = val_array_size(values);
		pobj_class.shader_program.fragment_src.src = (LHchar **)malloc(sizeof(*pobj_class.shader_program.fragment_src.src) * pobj_class.shader_program.fragment_src.nb_lines);

		for(int i=0; i<pobj_class.shader_program.fragment_src.nb_lines; i++)
			pobj_class.shader_program.fragment_src.src[i] = (LHchar *)val_get_string(val_array_i(values, i));

		pobj_class.shader_program.draw_type = val_get_int(val_field(shader_program, val_id("draw_type")));

		values = val_field(shader_program, val_id("textures"));

		pobj_class.shader_program.nb_textures = val_array_size(values);
		pobj_class.shader_program.textures = (LHuint *)val_array_int(values);

		values = val_field(shader_program, val_id("custom_uniforms"));

		pobj_class.shader_program.nb_custom_uniforms = val_array_size(values);
		pobj_class.shader_program.custom_uniforms = (lh_custom_uniform_t *)malloc(sizeof(*pobj_class.shader_program.custom_uniforms) * pobj_class.shader_program.nb_custom_uniforms);

		for(int i=0; i<pobj_class.shader_program.nb_custom_uniforms; i++) {
			value cust_uniform = val_array_i(values, i);

			pobj_class.shader_program.custom_uniforms[i].name = (LHchar *)val_get_string(val_field(val_array_i(values, i), val_id("name")));
			pobj_class.shader_program.custom_uniforms[i].type = val_get_int(val_field(val_array_i(values, i), val_id("type")));
		}

		value buffers = val_field(obj_class, val_id("buffers"));

		

		pobj_class.allow_culling = val_get_bool(val_field(obj_class, val_id("allow_culling"))) ? LH_TRUE : LH_FALSE;

		return alloc_int(lhRegObjClass((lh_window_t *)val_get_handle(window, hx_lh_window_t_ptr), &pobj_class));
	}

	static value hx_lhRegObj(value window, value obj) {
		lh_obj_t pobj;
		value values;


	}

	/*
	extern void lhRegObj(lh_window_t *window, lh_obj_t *obj);
	extern void lhUnregObj(lh_window_t *window, lh_obj_t *obj);
	extern void lhUnregObjClass(lh_window_t *window, lh_obj_class_t *obj_class);

	extern LHint lhSetupCustomBuffer(lh_window_t *window, lh_obj_t *obj, LHint type);
	extern LHint lhUpdateCustomBuffer(lh_window_t *window, lh_obj_t *obj, LHint type);
	extern LHint lhDeleteCustomBuffer(lh_window_t *window, lh_obj_t *obj, LHint type);
	*/

	static value hx_lhDirtyObjBuffer(value window) {
		lhDirtyObjBuffer((lh_window_t *)val_get_handle(window, hx_lh_window_t_ptr));

		return alloc_null();
	}

	static value hx_lhIsDirtyObjBuffer(value window) {
		return alloc_bool(lhIsDirtyObjBuffer((lh_window_t *)val_get_handle(window, hx_lh_window_t_ptr)) ? true : false);
	}

	static value hx_lhDrawObjs(value window) {
		lhDrawObjs((lh_window_t *)val_get_handle(window, hx_lh_window_t_ptr));

		return alloc_null();
	}
}

DEFINE_PRIM(hx_lhCoreInit, 1)
DEFINE_PRIM(hx_lhGetRenderDistances, 1)
DEFINE_PRIM(hx_lhSetRenderDistances, 3)
DEFINE_PRIM(hx_lhGetBackColor, 1)
DEFINE_PRIM(hx_lhSetBackColor, 2)
DEFINE_PRIM(hx_lhGetAmbientLight, 1)
DEFINE_PRIM(hx_lhSetAmbientLight, 2)
DEFINE_PRIM(hx_lhEnableLight, 3)
DEFINE_PRIM(hx_lhSetLightParameters, 4)
DEFINE_PRIM(hx_lhGetCameraPos, 1)
DEFINE_PRIM(hx_lhSetCameraPos, 2)
DEFINE_PRIM(hx_lhGetCameraAngle, 1)
DEFINE_PRIM(hx_lhSetCameraAngle, 2)
DEFINE_PRIM(hx_lhEnableFog, 2)
DEFINE_PRIM(hx_lhSetFogParameters, 4)

DEFINE_PRIM(hx_lhLoadTextureFromImage, 2)
DEFINE_PRIM(hx_lhLoadTextureRGB, 4)
DEFINE_PRIM(hx_lhLoadTextureRGBA, 4)

DEFINE_PRIM(hx_lhRegObjClass, 2)


DEFINE_PRIM(hx_lhDirtyObjBuffer, 1)
DEFINE_PRIM(hx_lhIsDirtyObjBuffer, 1)
DEFINE_PRIM(hx_lhDrawObjs, 1)

