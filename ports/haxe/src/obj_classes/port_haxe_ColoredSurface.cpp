/*
 * Copyright (C) 2014 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <hx/CFFI.h>

#include <libhang/core.h>
#include <libhang/obj_classes/ColoredSurface.h>

#include "../kinds.h"

extern "C" {
	static value hx_lhRegColoredSurface(value window) {
		return alloc_int(lhRegColoredSurface((lh_window_t *)val_get_handle(window, hx_lh_window_t_ptr)));
	}

	static value hx_lhUnregColoredSurface(value window) {
		lhUnregColoredSurface((lh_window_t *)val_get_handle(window, hx_lh_window_t_ptr));

		return alloc_null();
	}

	static value hx_lhNewColoredSurface(value *args, int count) {
		if(count != 9)
			return alloc_null();

		lh_obj_t *ret = lhNewColoredSurface((lh_window_t *)val_get_handle(args[0], hx_lh_window_t_ptr), val_get_double(args[1]), val_get_int(args[2]), val_get_int(args[3]), val_array_float(args[4]), (lh_vec3_t *)val_array_double(args[5]), (lh_vec3_t *)val_array_double(args[6]), (lh_rgba_t *)val_array_double(args[7]), val_get_bool(args[8]) ? LH_TRUE : LH_FALSE);

		if(ret)
			return alloc_abstract(hx_lh_obj_t_ptr, ret);

		return alloc_null();
	}

	static value hx_lhDeleteColoredSurface(value window, value surface) {
		lhDeleteColoredSurface((lh_window_t *)val_get_handle(window, hx_lh_window_t_ptr), (lh_obj_t *)val_get_handle(surface, hx_lh_obj_t_ptr));

		return alloc_null();
	}

	static value hx_lhColoredSurfaceUploadVertices(value window, value surface, value vertices) {
		lhColoredSurfaceUploadVertices((lh_window_t *)val_get_handle(window, hx_lh_window_t_ptr), (lh_obj_t *)val_get_handle(surface, hx_lh_obj_t_ptr), val_array_float(vertices));

		return alloc_null();
	}
}

DEFINE_PRIM(hx_lhRegColoredSurface, 1)
DEFINE_PRIM(hx_lhUnregColoredSurface, 1)
DEFINE_PRIM_MULT(hx_lhNewColoredSurface)
DEFINE_PRIM(hx_lhDeleteColoredSurface, 2)
DEFINE_PRIM(hx_lhColoredSurfaceUploadVertices, 3)

