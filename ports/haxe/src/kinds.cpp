/*
 * Copyright (C) 2014 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <hx/CFFI.h>

#include <libhang/core.h>

#include "kinds.h"

extern "C" {
	static int kinds_init = 0;

	static value hx_lhSetupKinds(void) {
		if(kinds_init)
			return alloc_null();

		hx_lh_window_t_ptr = alloc_kind();

		hx_lh_event_t_ptr = alloc_kind();
		hx_lh_thread_t_ptr = alloc_kind();

		hx_lh_image_t_ptr = alloc_kind();

		kinds_init = 1;
	}
}

DEFINE_PRIM(hx_lhSetupKinds, 0)

