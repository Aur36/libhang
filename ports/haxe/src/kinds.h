/*
 * Copyright (C) 2014 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

/* core.h */
DECLARE_KIND(hx_lh_window_t_ptr)
DECLARE_KIND(hx_lh_obj_class_t_ptr)
DECLARE_KIND(hx_lh_obj_t_ptr)

/* driver.h */
DECLARE_KIND(hx_lh_event_t_ptr)
DECLARE_KIND(hx_lh_thread_t_ptr)

/* images.h */
DECLARE_KIND(hx_lh_image_t_ptr)
