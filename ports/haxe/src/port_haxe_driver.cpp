/*
 * Copyright (C) 2014 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <hx/CFFI.h>

#include <libhang/core.h>

#include "kinds.h"

DEFINE_KIND(hx_lh_event_t_ptr)
DEFINE_KIND(hx_lh_thread_t_ptr)

extern "C" {
	static value hx_lhInit(void) {
		return alloc_int(lhInit());
	}

	static value hx_lhDelay(value millis) {
		lhDelay(val_get_int(millis));

		return alloc_null();
	}

	static value hx_lhSwapBuffers(value window) {
		lhSwapBuffers((lh_window_t *)val_get_handle(window, hx_lh_window_t_ptr));

		return alloc_null();
	}

	static value hx_lhAcquireGraphicCtx(value window) {
		lhAcquireGraphicCtx((lh_window_t *)val_get_handle(window, hx_lh_window_t_ptr));

		return alloc_null();
	}

	static value hx_lhReleaseGraphicCtx(value window) {
		lhReleaseGraphicCtx((lh_window_t *)val_get_handle(window, hx_lh_window_t_ptr));

		return alloc_null();
	}

	static value hx_lhCreateWindow(value *args, int count) {
		if(count != 6)
			return val_null;
		
		lh_window_t *ret = lhCreateWindow(val_get_string(args[0]), val_get_int(args[1]), val_get_int(args[2]), val_get_int(args[3]), val_get_int(args[4]), val_get_int(args[5]));
		
		if(ret)
			return alloc_abstract(hx_lh_window_t_ptr, ret);
		
		return alloc_null();
	}

	static value hx_lhStartDrawing(value window) {
		lhStartDrawing((lh_window_t *)val_get_handle(window, hx_lh_window_t_ptr));

		return alloc_null();
	}

	static value hx_lhStopDrawing(value window) {
		lhStopDrawing((lh_window_t *)val_get_handle(window, hx_lh_window_t_ptr));

		return alloc_null();
	}

	static value hx_lhEnterEventLoop(value window) {
		lhEnterEventLoop((lh_window_t *)val_get_handle(window, hx_lh_window_t_ptr));

		return alloc_null();
	}

	static value hx_lhLeaveEventLoop(value window) {
		lhLeaveEventLoop((lh_window_t *)val_get_handle(window, hx_lh_window_t_ptr));

		return alloc_null();
	}

	static value hx_lhDestroyWindow(value window) {
		return alloc_int(lhDestroyWindow((lh_window_t *)val_get_handle(window, hx_lh_window_t_ptr)));
	}

	struct event_handler_params {
		value real_handler;
		value params;
	};

	static void event_handler(lh_event_t *event, void *pparams) {
		struct event_handler_params *params = (struct event_handler_params *)pparams;
		
		val_call2(params->real_handler, alloc_abstract(hx_lh_event_t_ptr, event), params->params);
	}

	static value hx_lhAddEventHandler(value window, value handler, value pparams) {
		val_check_function(handler, 2);
		
		event_handler_params *params = (struct event_handler_params *)malloc(sizeof(*params));
		if(!params)
			return alloc_int(-1);

		params->real_handler = handler;
		params->params = pparams;

		return alloc_int(lhAddEventHandler((lh_window_t *)val_get_handle(window, hx_lh_window_t_ptr), event_handler, (void *)params));
	}

	static value hx_lhRemoveEventHandler(value window, value id) {
		lhRemoveEventHandler((lh_window_t *)val_get_handle(window, hx_lh_window_t_ptr), val_get_int(id));
		
		return alloc_null();
	}

	struct thread_func_params {
		value real_func;
		value params;
	};

	static void *thread_func(void *pparams) {
		thread_func_params *params = (thread_func_params *)pparams;

		val_call1(params->real_func, params->params);
	}

	static value hx_lhCreateThread(value func, value pparams) {
		val_check_function(func, 1);
		
		thread_func_params *params = (struct thread_func_params *)malloc(sizeof(*params));
		if(!params)
			return alloc_null();

		params->real_func = func;
		params->params = pparams;
		
		lh_thread_t *ret = lhCreateThread(thread_func, params);
		if(ret)
			return alloc_abstract(hx_lh_thread_t_ptr, ret);
		
		return alloc_null();
	}

	static value hx_lhWaitThread(value thread) {
		lhWaitThread((lh_thread_t *)val_get_handle(thread, hx_lh_thread_t_ptr));
		
		return alloc_null();
	}

	static value hx_lhDestroyThread(value thread) {
		lhDestroyThread((lh_thread_t *)val_get_handle(thread, hx_lh_thread_t_ptr));
		
		return alloc_null();
	}
}

DEFINE_PRIM(hx_lhInit, 0)
DEFINE_PRIM(hx_lhDelay, 1)
DEFINE_PRIM(hx_lhSwapBuffers, 1)
DEFINE_PRIM(hx_lhAcquireGraphicCtx, 1)
DEFINE_PRIM(hx_lhReleaseGraphicCtx, 1)
DEFINE_PRIM_MULT(hx_lhCreateWindow)
DEFINE_PRIM(hx_lhStartDrawing, 1)
DEFINE_PRIM(hx_lhStopDrawing, 1)
DEFINE_PRIM(hx_lhEnterEventLoop, 1)
DEFINE_PRIM(hx_lhLeaveEventLoop, 1)
DEFINE_PRIM(hx_lhDestroyWindow, 1)
DEFINE_PRIM(hx_lhAddEventHandler, 3)
DEFINE_PRIM(hx_lhRemoveEventHandler, 2)
DEFINE_PRIM(hx_lhCreateThread, 2)
DEFINE_PRIM(hx_lhWaitThread, 1)
DEFINE_PRIM(hx_lhDestroyThread, 1)

