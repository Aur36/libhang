/*
 * Copyright (C) 2014 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

class LibHang
{
	/* Driver */

	private static var lhSetupKinds:Void->Dynamic = cpp.Lib.load("libhang-haxe", "hx_lhSetupKinds", 0);
	static var lhInit:Void->Int = cpp.Lib.load("libhang-haxe", "hx_lhInit", 0);
	static var lhDelay:Int->Dynamic = cpp.Lib.load("libhang-haxe", "hx_lhDelay", 1);
	static var lhSwapBuffers:Dynamic->Dynamic = cpp.Lib.load("libhang-haxe", "hx_lhSwapBuffers", 1);
	static var lhAcquireGraphicCtx:Dynamic->Dynamic = cpp.Lib.load("libhang-haxe", "hx_lhAcquireGraphicCtx", 1);
	static var lhReleaseGraphicCtx:Dynamic->Dynamic = cpp.Lib.load("libhang-haxe", "hx_lhReleaseGraphicCtx", 1);
	static var lhCreateWindow:String->Int->Int->Int->Int->Int->Dynamic = cpp.Lib.load("libhang-haxe", "hx_lhCreateWindow", 6);
	static var lhStartDrawing:Dynamic->Dynamic = cpp.Lib.load("libhang-haxe", "hx_lhStartDrawing", 1);
	static var lhStopDrawing:Dynamic->Dynamic = cpp.Lib.load("libhang-haxe", "hx_lhStopDrawing", 1);
	static var lhEnterEventLoop:Dynamic->Dynamic = cpp.Lib.load("libhang-haxe", "hx_lhEnterEventLoop", 1);
	static var lhLeaveEventLoop:Dynamic->Dynamic = cpp.Lib.load("libhang-haxe", "hx_lhLeaveEventLoop", 1);
	static var lhDestroyWindow:Dynamic->Int = cpp.Lib.load("libhang-haxe", "hx_lhDestroyWindow", 1);
	static var lhAddEventHandler:Dynamic->Dynamic->Dynamic->Int = cpp.Lib.load("libhang-haxe", "hx_lhAddEventHandler", 3);
	static var lhRemoveEventHandler:Dynamic->Int->Dynamic = cpp.Lib.load("libhang-haxe", "hx_lhRemoveEventHandler", 2);
	static var lhCreateThread:Dynamic->Dynamic->Dynamic = cpp.Lib.load("libhang-haxe", "hx_lhCreateThread", 2);
	static var lhWaitThread:Dynamic->Dynamic = cpp.Lib.load("libhang-haxe", "hx_lhWaitThread", 1);
	static var lhDestroyThread:Dynamic->Dynamic = cpp.Lib.load("libhang-haxe", "hx_lhDestroyThread", 1);

	/* Core */

	static var lhCoreInit:Dynamic->Dynamic = cpp.Lib.load("libhang-haxe", "hx_lhCoreInit", 1);
	static var lhGetRenderDistances:Dynamic->Dynamic = cpp.Lib.load("libhang-haxe", "hx_lhGetRenderDistances", 1);
	static var lhSetRenderDistances:Dynamic->Float->Float->Dynamic = cpp.Lib.load("libhang-haxe", "hx_lhSetRenderDistances", 3);
	static var lhGetBackColor:Dynamic->Dynamic = cpp.Lib.load("libhang-haxe", "hx_lhGetBackColor", 1);
	static var lhSetBackColor:Dynamic->Dynamic->Dynamic = cpp.Lib.load("libhang-haxe", "hx_lhSetBackColor", 2);
	static var lhGetAmbientLight:Dynamic->Dynamic = cpp.Lib.load("libhang-haxe", "hx_lhGetAmbientLight", 1);
	static var lhSetAmbientLight:Dynamic->Dynamic->Dynamic = cpp.Lib.load("libhang-haxe", "hx_lhSetAmbientLight", 2);
	static var lhEnableLight:Dynamic->Int->Bool->Dynamic = cpp.Lib.load("libhang-haxe", "hx_lhEnableLight", 3);
	static var lhSetLightParameters:Dynamic->Int->Dynamic->Dynamic->Dynamic = cpp.Lib.load("libhang-haxe", "hx_lhSetLightParameters", 4);
	static var lhGetCameraPos:Dynamic->Dynamic = cpp.Lib.load("libhang-haxe", "hx_lhGetCameraPos", 1);
	static var lhSetCameraPos:Dynamic->Dynamic->Dynamic = cpp.Lib.load("libhang-haxe", "hx_lhSetCameraPos", 2);
	static var lhGetCameraAngle:Dynamic->Dynamic = cpp.Lib.load("libhang-haxe", "hx_lhGetCameraAngle", 1);
	static var lhSetCameraAngle:Dynamic->Dynamic->Dynamic = cpp.Lib.load("libhang-haxe", "hx_lhSetCameraAngle", 2);
	static var lhEnableFog:Dynamic->Bool->Dynamic = cpp.Lib.load("libhang-haxe", "hx_lhEnableFog", 2);
	static var lhSetFogParameters:Dynamic->Float->Float->Dynamic->Dynamic = cpp.Lib.load("libhang-haxe", "hx_lhSetFogParameters", 4);

	static var lhLoadTextureFromImage:Dynamic->Dynamic->Int = cpp.Lib.load("libhang-haxe", "hx_lhLoadTextureFromImage", 2);
	static var lhLoadTextureRGB:Dynamic->Dynamic->Int->Int->Int = cpp.Lib.load("libhang-haxe", "hx_lhLoadTextureRGB", 4);
	static var lhLoadTextureRGBA:Dynamic->Dynamic->Int->Int->Int = cpp.Lib.load("libhang-haxe", "hx_lhLoadTextureRGBA", 4);

	/* static var lhRegObjClass:Dynamic->Dynamic->Int = cpp.Lib.load("libhang-haxe", "hx_lhRegObjClass", 2); */
	static var lhRegObjClass:Dynamic->Dynamic->Int = function(window:Dynamic, obj_class:Dynamic):Int { throw "lhRegObjClass: Err. not implemented."; return -1; };
	/* static var lhRegObj:Dynamic->Dynamic->Dynamic = cpp.Lib.load("libhang-haxe", "hx_lhRegObj", 2); */
	static var lhRegObj:Dynamic->Dynamic->Dynamic = function(window:Dynamic, obj:Dynamic):Dynamic { throw "lhRegObj: Err. not implemented."; return null; };
	/* static var lhUnregObj:Dynamic->Dynamic->Dynamic = cpp.Lib.load("libhang-haxe", "hx_lhUnregObj", 2); */
	static var lhUnregObj:Dynamic->Dynamic->Dynamic = function(window:Dynamic, obj:Dynamic):Dynamic { throw "lhUnregObj: Err. not implemented."; return null; };
	/* static var lhUnregObjClass:Dynamic->Dynamic->Dynamic = cpp.Lib.load("libhang-haxe", "hx_lhUnregObjClass", 2); */
	static var lhUnregObjClass:Dynamic->Dynamic->Int = function(window:Dynamic, obj_class:Dynamic):Dynamic { throw "lhUnregObjClass: Err. not implemented."; return null; };

	/* static var lhSetupCustomBuffer:Dynamic->Dynamic->Int->Int = cpp.Lib.load("libhang-haxe", "hx_lhSetupCustomBuffer", 3); */
	static var lhSetupCustomBuffer:Dynamic->Dynamic->Int->Int = function(window:Dynamic, obj:Dynamic, type:Int):Int { throw "lhSetupCustomBuffer: Err. not implemented."; return -1; };
	/* static var lhUpdateCustomBuffer:Dynamic->Dynamic->Int->Int = cpp.Lib.load("libhang-haxe", "hx_lhUpdateCustomBuffer", 3); */
	static var lhUpdateCustomBuffer:Dynamic->Dynamic->Int->Int = function(window:Dynamic, obj:Dynamic, type:Int):Int { throw "lhUpdateCustomBuffer: Err. not implemented."; return -1; };
	/* static var lhDeleteCustomBuffer:Dynamic->Dynamic->Int->Int = cpp.Lib.load("libhang-haxe", "hx_lhDeleteCustomBuffer", 3); */
	static var lhDeleteCustomBuffer:Dynamic->Dynamic->Int->Int = function(window:Dynamic, obj:Dynamic, type:Int):Int { throw "lhDeleteCustomBuffer: Err. not implemented."; return -1; };

	/* Images */

	static var lhLoadImagePNG:Dynamic->Dynamic = cpp.Lib.load("libhang-haxe", "hx_lhLoadImagePNG", 1);
	static var lhLoadImage:Dynamic->Dynamic = cpp.Lib.load("libhang-haxe", "hx_lhLoadImage", 1);
	static var lhReleaseImage:Dynamic->Dynamic = cpp.Lib.load("libhang-haxe", "hx_lhReleaseImage", 1);

	/* ColoredCube */

	static var lhRegColoredCube:Dynamic->Int = cpp.Lib.load("libhang-haxe", "hx_lhRegColoredCube", 1);
	static var lhUnregColoredCube:Dynamic->Dynamic = cpp.Lib.load("libhang-haxe", "hx_lhUnregColoredCube", 1);
	static var lhNewColoredCube:Dynamic->Float->Dynamic->Dynamic->Dynamic->Bool->Dynamic = cpp.Lib.load("libhang-haxe", "hx_lhNewColoredCube", 6);
	static var lhDeleteColoredCube:Dynamic->Dynamic->Dynamic = cpp.Lib.load("libhang-haxe", "hx_lhDeleteColoredCube", 2);

	/* ColoredSurface */

	static var lhRegColoredSurface:Dynamic->Int = cpp.Lib.load("libhang-haxe", "hx_lhRegColoredSurface", 1);
	static var lhUnregColoredSurface:Dynamic->Dynamic = cpp.Lib.load("libhang-haxe", "hx_lhUnregColoredSurface", 1);
	static var lhNewColoredSurface:Dynamic->Float->Int->Int->Dynamic->Dynamic->Dynamic->Dynamic->Bool->Dynamic = cpp.Lib.load("libhang-haxe", "hx_lhNewColoredSurface", 9);
	static var lhDeleteColoredSurface:Dynamic->Dynamic->Dynamic = cpp.Lib.load("libhang-haxe", "hx_lhDeleteColoredSurface", 2);
	static var lhColoredSurfaceUploadVertices:Dynamic->Dynamic->Dynamic->Dynamic = cpp.Lib.load("libhang-haxe", "hx_lhColoredSurfaceUploadVertices", 3);

	static function main()
	{
		var window;
		var ev_handler_id=-1;
		var test_thread_id=null;
		lhSetupKinds();

		trace(lhInit());

		window = lhCreateWindow("Render", 0, 0, 600, 600, 0);
		trace(window);

		trace(lhSetRenderDistances(window, 0.1, 100.0));
		trace(lhGetRenderDistances(window));

		trace(lhSetBackColor(window, {r:0.0, g:0.0, b:0.1, a:1.0}));
		trace(lhGetBackColor(window));

		trace(lhSetAmbientLight(window, {r:0.1, g:0.1, b:0.1}));
		trace(lhGetAmbientLight(window));

		ev_handler_id = lhAddEventHandler(window, event_handler, window);
		trace(ev_handler_id);

		test_thread_id = lhCreateThread(test_thread, "Thread param");
		trace(test_thread_id);

		trace(lhEnableLight(window, 0, true));
		trace(lhSetLightParameters(window, 0, {r:.5, g:.5, b:.5}, {x:-1.0, y:-1.0, z:-1.0}));

		trace(lhSetCameraPos(window, {x:0., y:0., z:0.}));
		trace(lhGetCameraPos(window));

		trace(lhSetCameraAngle(window, {x:0., y:0., z:0.}));
		trace(lhGetCameraAngle(window));

		trace(lhEnableFog(window, true));
		trace(lhSetFogParameters(window, 99., 100., {r:0.0, g:0.0, b:0.1}));

		var rock = lhLoadImage("rock.png");
		trace(rock);
		if(rock != null) {
			var rock_texture_id = lhLoadTextureFromImage(window, rock);
			trace(rock_texture_id);
			trace(lhReleaseImage(rock));
		}

		/* trace(lhRegObjClass(window, {
			name: "Dummy",
			shader_program: {
				vertex_src: {
					src: [ "", "a" ]
				},
				fragment_src: {
					src: [ "" ]
				},
				draw_type: 1,
				textures: null,
				custom_uniforms: null,
			}
		})); */

		trace(lhRegColoredCube(window));
		trace(lhRegColoredSurface(window));

		trace(lhNewColoredCube(window, 0.5, {x:0.0, y:0.0, z:-2.0}, {x:0.0, y:0.5, z:0.0}, {r:0.5, g:0.2, b:0.1, a:1.0}, true));

		trace(lhStartDrawing(window));
		trace(lhEnterEventLoop(window));

		trace(lhDestroyThread(test_thread_id));

		trace(lhDestroyWindow(window));
	}

	static function test_thread(params:Dynamic)
	{
		trace(params);

		while(true) {
			lhDelay(3000);
			trace("Thread: Hint 3000ms");
		}
	}

	static function event_handler(event:Dynamic, params:Dynamic)
	{
		trace("Event");
	}
}
