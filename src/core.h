/*
 * Copyright (C) 2015-2016 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 3 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef LIBHANG_INTERNAL_CORE_H
#define LIBHANG_INTERNAL_CORE_H 1

#include <libhang.h>

enum lh_obj_class_private_flags {
	LH_OBJ_CLASS_INTERNAL_FLAG = (1 << 0) /* The obj_class is internal to libHang, and must be rendered in a strategic place */
};

extern int lhCoreInit(struct lh_window *window);
extern int lhCoreDeinit(struct lh_window *window);

#endif /* LIBHANG_INTERNAL_CORE_H */
