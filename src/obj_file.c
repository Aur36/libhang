/*
 * Copyright (C) 2015-2016 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 3 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <ctype.h>

#include <libhang.h>

#define READ_ONCE 256


static struct lh_obj_class Sample_obj_class = {
	.name="Sample",

	.uses = LH_BUF_POS_FLAG
			| LH_BUF_NORMAL_FLAG
			| LH_BUF_INDEX_FLAG,
	.overrides = 0,

	.shader_program = {
		.vertex_src = {
			.src = (char *[47]){
				"#version 130 \n",
				" \n",
				"in vec3 a_VertexPosition; \n",
				"in vec3 a_VertexNormal; \n",
				"in mat4 a_MVMatrix; \n",
				" \n",
				"uniform bool u_IsInstanced; \n",
				"uniform mat4 u_MVMatrix; \n",
				"uniform mat4 u_PMatrix; \n",
				"uniform mat3 u_NMatrix; \n",
				" \n",
				"uniform vec3 u_AmbientLight; \n",
				" \n",
				"uniform vec3 u_LightColor; \n",
				"uniform vec3 u_LightDirection; \n",
				" \n",
				"uniform vec3 u_AbsPosition; \n",
				"uniform vec3 u_CameraPosition; \n",
				" \n",
				"uniform bool u_FogEnabled; \n",
				"uniform float u_FogMinDistance; \n",
				"uniform float u_FogMaxDistance; \n",
				" \n",
				"uniform float u_SideLength; \n",
				" \n",
				"out vec3 v_LightWeighting; \n",
				" \n",
				"out float v_FogWeighting; \n",
				" \n",
				"void main(void) { \n",
				"	mat4 mvMatrix = u_IsInstanced ? a_MVMatrix : u_MVMatrix; \n",
				"	gl_Position = u_PMatrix * mvMatrix * vec4(a_VertexPosition, 1.0); \n",
				" \n",
				"	float CameraDistance = length(u_AbsPosition + mat3(mvMatrix) * a_VertexPosition * u_SideLength - u_CameraPosition); \n",
				"	//mat3 u_NMatrix = mat3(inverse(transpose(mvMatrix))); \n",
				"	v_LightWeighting = min(u_AmbientLight + u_LightColor * max(dot(u_NMatrix * a_VertexNormal, -normalize(u_LightDirection)), 0.0), vec3(1.0, 1.0, 1.0)); \n",
				"	float FogDistance = CameraDistance; \n",
				"	if(u_FogEnabled) { \n",
				"		// v_FogWeighting = (FogDistance - u_FogMinDistance) / (u_FogMaxDistance - u_FogMinDistance); \n",
				"		// v_FogWeighting = sin(((FogDistance - u_FogMinDistance) / (u_FogMaxDistance - u_FogMinDistance)) * asin(1.0)); \n",
				"		// v_FogWeighting = (exp((FogDistance - u_FogMinDistance) / (u_FogMaxDistance - u_FogMinDistance)) - 1.0) / (exp(1.0) - 1.0); \n",
				"		v_FogWeighting = log((FogDistance - u_FogMinDistance) / (u_FogMaxDistance - u_FogMinDistance) + 1.0) / log(2.0); \n",
				"		v_FogWeighting = clamp(v_FogWeighting, 0.0, 1.0); \n",
				"	} else { \n",
				"		v_FogWeighting = 0.0; \n",
				"	} \n",
				"} \n"
			},
			.nb_lines = 47,
		},
		.fragment_src = {
			.src = (char *[18]){
				"#version 130 \n",
				" \n",
				"out vec4 outColor; \n",
				" \n",
				"in vec3 v_LightWeighting; \n",
				"in float v_FogWeighting; \n",
				" \n",
				"uniform vec4 u_Color; \n",
				" \n",
				"uniform bool u_FogEnabled; \n",
				"uniform vec3 u_FogColor; \n",
				" \n",
				"void main(void) { \n",
				"	vec4 color = vec4(u_Color.rgb * v_LightWeighting, 1.0); \n",
				"	if(u_FogEnabled) { \n",
				"		color = vec4((1.0 - v_FogWeighting) * color.rgb + v_FogWeighting * u_FogColor, color.a); \n",
				"	} \n"
				"	outColor = color; \n",
				"} \n"
			},
			.nb_lines = 18,
		},
		.drawing_type = LH_DRAW_TRIANGLES,

		.nb_custom_uniforms = 0,
		.custom_uniforms = (struct lh_custom_uniform []){
			{ .name = "u_SideLength", .type = LH_TYPE_FLOAT },
			{ .name = "u_Color",      .type = LH_TYPE_VEC4  },
		},
	},
	.buffers = {
		[LH_BUF_POS] = { .nb_items = 0, .data = NULL },
		[LH_BUF_NORMAL] = { .nb_items = 0, .data = NULL },
		[LH_BUF_INDEX] = { .nb_items = 0, .data = NULL },
	},
};

static char *read_text_file(const char *path)
{
	FILE *fp;
	char *data = NULL;
	int data_len = 0;
	int data_idx = 0;
	void *ret;

	fp = fopen(path, "r");
	if(!fp)
		return NULL;

	while(!feof(fp) && !ferror(fp))
	{
		data_len += READ_ONCE - 1;
		char *tmp_ptr = realloc(data, data_len);
		if(data && !tmp_ptr) {
			free(data);
			fclose(fp);
			return NULL;
		}
		data = tmp_ptr;
		ret = fgets(&data[data_idx], READ_ONCE, fp);
		if(!ret)
			break;
		data_idx += strlen(&data[data_idx]);
	}
	if(ferror(fp)) {
		fclose(fp);
		return NULL;
	}

	fclose(fp);
	return data;
}

struct lh_obj_class **lhCreateObjClassesFromOBJ(const char *data)
{
	int obj_classes_idx = 0;
	struct lh_obj_class **obj_classes = NULL;
	uintptr_t idx = 0;
	int cur_line = 1;
	lh_vec3_t *vertices = NULL;
	int vertices_idx = 0;
	lh_vec3_t *normals = NULL;
	int normals_idx = 0;
	lh_vec2_t *tex_coords = NULL;
	int tex_coords_idx = 0;
	LHuint *indices = NULL;
	int indices_idx = 0;

	if(!data)
		return NULL;

	obj_classes = malloc(sizeof(*obj_classes));
	if(!obj_classes)
		return NULL;
	obj_classes[obj_classes_idx] = NULL;

	while(data[idx])
	{
		switch(data[idx++]) {
			case '#': {
				/* Comment, skip the whole line */
				while(isprint(data[idx]) && data[idx] != '\n')
					idx++;
				break;
			}

			case 'm': {
				if(strncmp(&data[idx], "mtllib", 6)) {
					fprintf(stderr, "libhang: Unsupported directive \"mtllib\".\n");
					while(isprint(data[idx]) && data[idx] != '\n') /* Unsupported, ignore */
						idx++;
					break;
				}
				break;
			}

			case 'o': {
				const char *str;
				uintptr_t str_len;

				while(isspace(data[idx]) && data[idx] != '\n')
					idx++;
				if(data[idx] == '\n' || data[idx] == '\0') {
					fprintf(stderr, "libhang: Unterminated directive.\n");
					break;
				}
				str = &data[idx];
				while(isgraph(data[idx]))
					idx++;
				str_len = &data[idx] - str;

				obj_classes[obj_classes_idx] = malloc(sizeof(**obj_classes));
				obj_classes[obj_classes_idx]->name = malloc(str_len + 1);
				strncpy(obj_classes[obj_classes_idx]->name, str, str_len);
				obj_classes[obj_classes_idx]->name[str_len] = '\0';

				fprintf(stdout, "libhang: New obj_class : %s.\n", obj_classes[obj_classes_idx]->name);

				obj_classes_idx++;
				obj_classes = realloc(obj_classes, sizeof(*obj_classes) * (obj_classes_idx + 1));
				obj_classes[obj_classes_idx] = NULL;
				break;
			}

			case 'f': {
				const char *str;
				long idc[4];
				int nidc;

				for(nidc = 0;  nidc < 4; nidc++) {
					char *str_end;

					while(isspace(data[idx]) && data[idx] != '\n')
						idx++;
					if(nidc == 0 && (data[idx] == '\n' || data[idx] == '\0')) {
						fprintf(stderr, "libhang: Unterminated directive.\n");
						break;
					}

					str = &data[idx];
					while(isdigit(data[idx]))
						idx++;

					idc[nidc] = strtol(str, &str_end, 10);
					if(str_end != &data[idx]) {
						fprintf(stderr, "libhang: Unexpected symbol in directive f.\n");
						break;
					}

					str = &data[idx];
				}

				while(isspace(data[idx]) && data[idx] != '\n')
					idx++;
				if(data[idx] != '\n' && data[idx] != '\0')
					break;

				indices = realloc(indices, sizeof(*indices) * (indices_idx + nidc));

				for(int i = 0; i < nidc; i++)
					indices[indices_idx + i] = idc[i];

				indices_idx += nidc;
				break;
			}

			case 'v': {
				const char *str;
				float v[3];

				for(int i=0;  i<3; i++) {
					char *str_end;

					while(isspace(data[idx]) && data[idx] != '\n')
						idx++;
					if(data[idx] == '\n' || data[idx] == '\0') {
						fprintf(stderr, "libhang: Unterminated directive.\n");
						break;
					}

					str = &data[idx];
					while(data[idx] == '-' || data[idx] == '.' || isdigit(data[idx]))
						idx++;

					v[i] = strtof(str, &str_end);
					if(str_end != &data[idx]) {
						fprintf(stderr, "libhang: Unexpected symbol in directive v.\n");
						break;
					}

					str = &data[idx];
				}

				while(isspace(data[idx]) && data[idx] != '\n')
					idx++;
				if(data[idx] != '\n' && data[idx] != '\0')
					break;

				vertices = realloc(vertices, sizeof(*vertices) * (vertices_idx + 1));

				vertices[vertices_idx].a[0] = v[0];
				vertices[vertices_idx].a[1] = v[1];
				vertices[vertices_idx].a[2] = v[2];

				vertices_idx++;
				break;
			}

			case 's': {
				fprintf(stderr, "libhang: Unsupported directive \"s\".\n");
				while(isprint(data[idx]) && data[idx] != '\n') /* Unsupported, ignore */
					idx++;
				break;
			}

			case 'u': {
				if(strncmp(&data[idx], "usemtl", 6)) {
					fprintf(stderr, "libhang: Unsupported directive \"usemtl\".\n");
					while(isprint(data[idx]) && data[idx] != '\n') /* Unsupported, ignore */
						idx++;
					break;
				}
				break;
			}
			
			default:
				fprintf(stderr, "libhang: (%d) Unsupported unknown directive.\n", cur_line);
		}

		/* Skip all the trailing spaces */
		while(isspace(data[idx]) && data[idx] != '\n')
			idx++;

		if(data[idx] != '\0' && data[idx] != '\n') {
			fprintf(stderr, "libhang: Error.\n");
			return NULL;
		}
		idx++;

		cur_line++;
	}

	if(!obj_classes[0])
		return NULL;

	for(int i=0; i<obj_classes_idx; i++) {
		if(!obj_classes[i]->name)
			obj_classes[i]->name = Sample_obj_class.name;
		obj_classes[i]->uses = Sample_obj_class.uses;
		obj_classes[i]->overrides = Sample_obj_class.overrides;
		obj_classes[i]->shader_program.vertex_src.src = Sample_obj_class.shader_program.vertex_src.src;
		obj_classes[i]->shader_program.vertex_src.nb_lines = Sample_obj_class.shader_program.vertex_src.nb_lines;
		obj_classes[i]->shader_program.fragment_src.src = Sample_obj_class.shader_program.fragment_src.src;
		obj_classes[i]->shader_program.fragment_src.nb_lines = Sample_obj_class.shader_program.fragment_src.nb_lines;
		obj_classes[i]->shader_program.drawing_type = Sample_obj_class.shader_program.drawing_type;
		obj_classes[i]->shader_program.nb_custom_uniforms = Sample_obj_class.shader_program.nb_custom_uniforms;
		obj_classes[i]->shader_program.custom_uniforms = Sample_obj_class.shader_program.custom_uniforms;
		obj_classes[i]->buffers[LH_BUF_POS].data = vertices;
		obj_classes[i]->buffers[LH_BUF_POS].nb_items = vertices_idx;
		obj_classes[i]->buffers[LH_BUF_NORMAL].data = normals;
		obj_classes[i]->buffers[LH_BUF_NORMAL].nb_items = normals_idx;
		obj_classes[i]->buffers[LH_BUF_TEX_COORD].data = tex_coords;
		obj_classes[i]->buffers[LH_BUF_TEX_COORD].nb_items = tex_coords_idx;
		obj_classes[i]->buffers[LH_BUF_MODELMATRIX].data = NULL;
		obj_classes[i]->buffers[LH_BUF_MODELMATRIX].nb_items = 0;
		obj_classes[i]->buffers[LH_BUF_INDEX].data = indices;
		obj_classes[i]->buffers[LH_BUF_INDEX].nb_items = indices_idx;
	}

	return obj_classes;
}

struct lh_obj_class **lhCreateObjClassesFromOBJFile(const char *path)
{
	struct lh_obj_class **obj_classes;
	char *data;

	data = read_text_file(path);
	if(!data)
		return NULL;

	obj_classes = lhCreateObjClassesFromOBJ(data);

	free(data);
	return obj_classes;
}

struct lh_obj_class **lhCreateObjClassesFromFile(const char *path)
{
	struct lh_obj_class **obj_classes;

	if((obj_classes = lhCreateObjClassesFromOBJFile(path)))
		return obj_classes;
	else
		return NULL;
}
