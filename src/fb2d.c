/*
 * Copyright (C) 2015-2016 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 3 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <libhang.h>

#include "core.h"
#include "fb2d.h"
#include "drivers/driver.h"

#define LH_FB2D_UPD_VISIBLE_FLAG LH_TO_FLAG(0)
#define LH_FB2D_UPD_Z_INDEX_FLAG LH_TO_FLAG(1)

#define LH_FB2D_UPD_OFF_FLAG     LH_TO_FLAG(0)
#define LH_FB2D_UPD_POS_FLAG     LH_TO_FLAG(1)
#define LH_FB2D_UPD_SIZE_FLAG    LH_TO_FLAG(2)

struct lh_fb2d_ctx {
	struct lh_fb2d_surface *window_surface;
	int internal_ev_handler_id;
	lh_ivec2_t pointed_pos;
	struct lh_fb2d_surface *pointed_surface;
};

struct lh_fb2d_surface {
	struct lh_obj base;
	LHuint nancestors;
	struct lh_fb2d_surface **ancestors;
	LHbool is_visible;
	uint64_t last_upd; /* The last time the surface was modified */
	uint64_t last_ack; /* The last time a change on the parent surface was acked */
	LHuint tex_width;
	LHuint tex_height;
	LHuint keep_ratio_flags;
	LHuint dst_mask;
	LHuint src_mask;
	struct lh_fb2d_clip dst_params;
	struct lh_fb2d_clip src_params;
	LHuint z_index;
	LHuint upd_mask;
	LHuint dst_upd_mask;
	LHuint src_upd_mask;
	LHuint computed_width;
	LHuint computed_height;
	LHuint nchilds;
	struct lh_fb2d_surface **childs;
};

static struct lh_obj_class FB2D_obj_class = {
	.name = "FB2D",

	.is_visible = LH_FALSE,
	.priv_flags = LH_OBJ_CLASS_INTERNAL_FLAG,
	.uses = LH_BUF_POS_FLAG
			| LH_BUF_TEX_COORD_FLAG
			| LH_BUF_INDEX_FLAG
			| LH_TEX_FLAG,
	.overrides = LH_TEX_FLAG,

	.shader_program = {
		.vertex_src = {
			.src = (char *[1]){
				"#version 150 \n"
				" \n"
				"in vec3 a_VertexPosition; \n"
				"in vec2 a_TextureCoord; \n"
				"out vec2 v_TextureCoord; \n"
				"uniform mat4 u_ModelMatrix; \n"
				"uniform vec2 u_DstOff; \n"
				"uniform vec4 u_DstRect; \n"
				"uniform float u_ZIndex; \n"
				" \n"
				"void main(void) { \n"
				"	vec4 vPos = u_ModelMatrix * vec4(a_VertexPosition, 1.f); \n"
				"	/* The Z-axis is directed towards the bottom of the screen, so we need to reverse the zIndex. */ \n"
				"	float zIndex = -u_ZIndex; \n"
				"	gl_Position = vec4((u_DstOff.x + u_DstRect.x) * 2.f + vPos.x * u_DstRect.z - 1.0, 1.0 - ((u_DstOff.y + u_DstRect.y) * 2.f + vPos.y * u_DstRect.w), zIndex, 1.0); \n"
				"	v_TextureCoord = a_TextureCoord; \n"
				"} \n"
			},
			.nb_lines = 1,
		},
		.fragment_src = {
			.src = (char *[1]){
				"#version 150 \n"
				" \n"
				"out vec4 outColor; \n"
				"in vec2 v_TextureCoord; \n"
				"uniform float u_GlobalAlpha; \n"
				"uniform vec2 u_SrcOff; \n"
				"uniform vec4 u_SrcRect; \n"
				"uniform sampler2D u_Sampler; \n"
				" \n"
				"void main(void) { \n"
				"	vec4 color = texture2D(u_Sampler, vec2(u_SrcOff.x + u_SrcRect.x + v_TextureCoord.s * u_SrcRect.z, u_SrcOff.y + u_SrcRect.y + v_TextureCoord.t * u_SrcRect.w)); \n"
				"	outColor = vec4(color.rgb, color.a * u_GlobalAlpha); \n"
				"} \n"
			},
			.nb_lines = 1,
		},
		.drawing_type = LH_DRAW_TRIANGLES,

		.nb_custom_uniforms = 7,
		.custom_uniforms = (struct lh_custom_uniform []){
#define UNIFORM_GLOBAL_ALPHA 0
			{ .name = "u_GlobalAlpha", .type = LH_TYPE_FLOAT },
#define UNIFORM_DST_OFF 1
			{ .name = "u_DstOff",      .type = LH_TYPE_VEC2  },
#define UNIFORM_DST_RECT 2
			{ .name = "u_DstRect",     .type = LH_TYPE_VEC4  },
#define UNIFORM_ZINDEX 3
			{ .name = "u_ZIndex",      .type = LH_TYPE_FLOAT },
#define UNIFORM_SRC_OFF 4
			{ .name = "u_SrcOff",      .type = LH_TYPE_VEC2  },
#define UNIFORM_SRC_RECT 5
			{ .name = "u_SrcRect",     .type = LH_TYPE_VEC4  },
#define UNIFORM_SAMPLER 6
			{ .name = "u_Sampler",     .type = LH_TYPE_INT   },
		},
	},
	.buffers = {
		[LH_BUF_POS] = {
			.nb_items = 4,
			.data = (float [3*4]){
				0., 0., 0.,
				2., 0., 0.,
				2., 2., 0.,
				0., 2., 0.,
			},
		},

		[LH_BUF_TEX_COORD] = {
			.nb_items = 4,
			.data = (float [2*4]){
				0.f, 0.f,
				1.f, 0.f,
				1.f, 1.f,
				0.f, 1.f,
			},
		},

		[LH_BUF_INDEX] = {
			.nb_items = 6,
			.data = (int [1*6]){
				0, 1, 2,   0, 2, 3,
			},
		},
	},

	.nb_textures = 1
};

static inline LHbool is_window_surface(struct lh_window *window, struct lh_fb2d_surface *surface) {
	return surface == window->core->fb2d->window_surface;
}

static LHuint on_event(struct lh_event *evt, void *param) {
	struct lh_window *window = param;
	struct lh_core_ctx *core = window->core;
	struct lh_fb2d_ctx *fb2d = core->fb2d;

	switch(evt->type) {
		case LH_EVENT_RESIZED:
			fb2d->window_surface->last_upd++;
			fb2d->window_surface->computed_width = window->width;
			fb2d->window_surface->computed_height = window->height;
			break;
		case LH_EVENT_MOUSE_MOVED: {
			struct lh_event_mouse_moved *mouse_evt = LH_CONTAINER_OF(evt, struct lh_event_mouse_moved, generic);
			fb2d->pointed_surface = NULL;
			fb2d->pointed_pos.c.x = mouse_evt->x;
			fb2d->pointed_pos.c.y = mouse_evt->y;
			break;
		}
	}
	return LH_EVENT_RET_UNHANDLED;
}

int lhFB2DInit(struct lh_window *window)
{
	struct lh_core_ctx *core = window->core;
	int ret;

	core->fb2d = malloc(sizeof(*core->fb2d));
	if(!core->fb2d)
		goto failed_alloc;

	ret = lhObjClassReg(window, &FB2D_obj_class);
	if(ret < 0)
		goto failed_reg;

	core->fb2d->window_surface = NULL;
	core->fb2d->window_surface = lhFB2DBlitSurface(
		window,
		NULL,
		LH_TEX_INVAL, 0, 0,
		0.f,
		LH_FB2D_KEEP_RATIO_NONE,
		LH_FB2D_PCT_ALL_FLAG, &LH_FB2D_CLIP(0.f, 0.f, 0.f, 0.f, 1.f, 1.f),
		LH_FB2D_PCT_ALL_FLAG, &LH_FB2D_CLIP(0.f, 0.f, 0.f, 0.f, 1.f, 1.f),
		1.f,
		LH_TRUE
	);
	if(!core->fb2d->window_surface)
		goto failed_win_surf;
	core->fb2d->window_surface->computed_width = window->width;
	core->fb2d->window_surface->computed_height = window->height;
	core->fb2d->window_surface->base.custom_uniforms_data[UNIFORM_ZINDEX].as_float = 0.f;

	core->fb2d->internal_ev_handler_id = lhAddInternalEventHandler(window, on_event, window);
	return 0;

failed_win_surf:
	lhObjClassUnreg(window, &FB2D_obj_class);
failed_reg:
	free(core->fb2d);
failed_alloc:
	return -1;
}

int lhFB2DDeinit(struct lh_window *window)
{
	struct lh_core_ctx *core = window->core;

	lhRemoveEventHandler(window, core->fb2d->internal_ev_handler_id);
	lhFB2DRemoveSurface(window, core->fb2d->window_surface);
	lhObjClassUnreg(window, &FB2D_obj_class);
	free(core->fb2d);
	core->fb2d = NULL;
	return 0;
}

struct lh_obj_class *lhFB2DGetObjClass(struct lh_window *window)
{
	lh_mark_used(window);

	return &FB2D_obj_class;
}

static inline void apply_keep_ratio(LHuint flags, LHuint *w, LHuint *h) {
	switch(flags) {
		case LH_FB2D_KEEP_RATIO_NONE:
			break;
		case LH_FB2D_KEEP_RATIO_X:
			*h = *w;
			break;
		case LH_FB2D_KEEP_RATIO_Y:
			*w = *h;
			break;
		case LH_FB2D_KEEP_RATIO_MIN:
			*h = *w = (*w < *h) ? *w : *h;
			break;
		case LH_FB2D_KEEP_RATIO_MAX:
			*h = *w = (*w > *h) ? *w : *h;
			break;
	}
}

static inline void apply_dst_mask(struct lh_window *window, struct lh_fb2d_surface *surface, LHuint pct_flag, float w, float h, float *v2, lh_vec2_t *dst_param, float *ancestor_v2) {
	if(surface->dst_mask & pct_flag) {
		v2[0] = floor(dst_param->c.x * w) / window->width;
		v2[1] = floor(dst_param->c.y * h) / window->height;
	} else {
		v2[0] = dst_param->c.x / window->width;
		v2[1] = dst_param->c.y / window->height;
	}
	v2[0] += ancestor_v2[0];
	v2[1] += ancestor_v2[1];
}

static inline void apply_src_mask(struct lh_fb2d_surface *surface, LHuint pct_flag, float *v2, lh_vec2_t *src_param) {
	if(surface->src_mask & pct_flag) {
		v2[0] = src_param->c.x;
		v2[1] = src_param->c.y;
	} else {
		v2[0] = src_param->c.x / surface->tex_width;
		v2[1] = src_param->c.y / surface->tex_height;
	}
}

void lhFB2DComputeCoords(struct lh_window *window, struct lh_obj *obj)
{
	struct lh_fb2d_surface *surface = LH_CONTAINER_OF(obj, struct lh_fb2d_surface, base);
	struct lh_fb2d_surface *parent;
	lh_vec4_t *v4;
	LHbool parent_updated = LH_FALSE;
	LHuint off_parent_w, off_parent_h;
	LHuint pos_parent_w, pos_parent_h;
	LHuint size_parent_w, size_parent_h;

	if(is_window_surface(window, surface))
		return;
	parent = surface->ancestors[0];

	for(LHuint i = surface->nancestors - 1; i >= 1; i--)
		if(surface->ancestors[i - 1]->last_ack != surface->ancestors[i]->last_upd)
			lhFB2DComputeCoords(window, &surface->ancestors[i - 1]->base);

	if(surface->last_ack != surface->ancestors[0]->last_upd)
		parent_updated = LH_TRUE;

	if(parent_updated || surface->upd_mask || surface->dst_upd_mask)
		window->core->fb2d->pointed_surface = NULL;

	if(parent_updated || surface->upd_mask & LH_FB2D_UPD_VISIBLE_FLAG)
		obj->is_visible = surface->is_visible && parent->is_visible && obj->custom_textures[0] != LH_TEX_INVAL;

	if(parent_updated || surface->upd_mask & LH_FB2D_UPD_Z_INDEX_FLAG) {
		float step = 1.f;
		const float parent_effective_z_index = parent->base.custom_uniforms_data[UNIFORM_ZINDEX].as_float;

		/*
		 * step is the distance between the effective z_indexes of parent's two consecutive brothers, as if:
		 *     step = parent->ancestors[0]->childs[1]->effective_z_index - parent->ancestors[0]->childs[0]->effective_z_index
		 * Note that it's the same distance as between the parent's lowest brother and its parent, which is used here:
		 *     step = parent->ancestors[0]->childs[0]->effective_z_index - parent->ancestors[0]->effective_z_index
		 */
		if(!is_window_surface(window, parent)) {
			const float first_parent_effective_z_index = parent->ancestors[0]->childs[0]->base.custom_uniforms_data[UNIFORM_ZINDEX].as_float;
			const float parent_ancestor_effective_z_index = parent->ancestors[0]->base.custom_uniforms_data[UNIFORM_ZINDEX].as_float;

			step = first_parent_effective_z_index - parent_ancestor_effective_z_index;
		}
		obj->custom_uniforms_data[UNIFORM_ZINDEX].as_float = parent_effective_z_index + step * ((float)(surface->z_index + 1) / (parent->nchilds + 1));
	}

	off_parent_w = parent->computed_width;
	off_parent_h = parent->computed_height;
	pos_parent_w = parent->computed_width;
	pos_parent_h = parent->computed_height;
	size_parent_w = parent->computed_width;
	size_parent_h = parent->computed_height;

	apply_keep_ratio((surface->keep_ratio_flags & LH_FB2D_KEEP_RATIO_OFF_MASK) >> LH_FB2D_KEEP_RATIO_OFF_SHIFT, &off_parent_w, &off_parent_h);
	apply_keep_ratio((surface->keep_ratio_flags & LH_FB2D_KEEP_RATIO_POS_MASK) >> LH_FB2D_KEEP_RATIO_POS_SHIFT, &pos_parent_w, &pos_parent_h);
	apply_keep_ratio((surface->keep_ratio_flags & LH_FB2D_KEEP_RATIO_SIZE_MASK) >> LH_FB2D_KEEP_RATIO_SIZE_SHIFT, &size_parent_w, &size_parent_h);

	if(parent_updated || surface->dst_upd_mask & LH_FB2D_UPD_OFF_FLAG)
		apply_dst_mask(
			window,
			surface,
			LH_FB2D_PCT_OFF_FLAG, off_parent_w, off_parent_h,
			obj->custom_uniforms_data[UNIFORM_DST_OFF].as_vec2.a,
			&surface->dst_params.off,
			parent->base.custom_uniforms_data[UNIFORM_DST_OFF].as_vec2.a
		);

	if(parent_updated || surface->dst_upd_mask & LH_FB2D_UPD_POS_FLAG)
		apply_dst_mask(
			window,
			surface,
			LH_FB2D_PCT_POS_FLAG, pos_parent_w, pos_parent_h,
			&obj->custom_uniforms_data[UNIFORM_DST_RECT].as_vec4.a[0],
			&surface->dst_params.pos,
			&parent->base.custom_uniforms_data[UNIFORM_DST_RECT].as_vec4.a[0]
		);

	if(parent_updated || surface->dst_upd_mask & LH_FB2D_UPD_SIZE_FLAG) {
		v4 = &obj->custom_uniforms_data[UNIFORM_DST_RECT].as_vec4;
		if(surface->dst_mask & LH_FB2D_PCT_SIZE_FLAG) {
			surface->computed_width = (LHuint)floor(surface->dst_params.size.c.x * size_parent_w);
			surface->computed_height = (LHuint)floor(surface->dst_params.size.c.y * size_parent_h);
		} else {
			surface->computed_width = (LHuint)floor(surface->dst_params.size.c.x);
			surface->computed_height = (LHuint)floor(surface->dst_params.size.c.y);
		}
		v4->c.z = (float)surface->computed_width / window->width;
		v4->c.w = (float)surface->computed_height / window->height;
	}

	if(surface->src_upd_mask & LH_FB2D_UPD_OFF_FLAG)
		apply_src_mask(
			surface,
			LH_FB2D_PCT_OFF_FLAG,
			obj->custom_uniforms_data[UNIFORM_SRC_OFF].as_vec2.a,
			&surface->src_params.off
		);

	if(surface->src_upd_mask & LH_FB2D_UPD_POS_FLAG)
		apply_src_mask(
			surface,
			LH_FB2D_PCT_POS_FLAG,
			&obj->custom_uniforms_data[UNIFORM_SRC_RECT].as_vec4.a[0],
			&surface->src_params.pos
		);

	if(surface->src_upd_mask & LH_FB2D_UPD_SIZE_FLAG)
		apply_src_mask(
			surface,
			LH_FB2D_PCT_SIZE_FLAG,
			&obj->custom_uniforms_data[UNIFORM_SRC_RECT].as_vec4.a[2],
			&surface->src_params.size
		);

	if(surface->dst_upd_mask || surface->src_upd_mask || parent_updated)
		surface->last_upd++;
	surface->last_ack = parent->last_upd;
	surface->dst_upd_mask = 0;
	surface->src_upd_mask = 0;
	surface->upd_mask = 0;
}

void lhFB2DSetVisible(LHbool is_visible)
{
	FB2D_obj_class.is_visible = is_visible;
}

struct lh_fb2d_surface *lhFB2DBlitSurface(struct lh_window *window, struct lh_fb2d_surface *parent_surface, LHuint texture, LHuint tex_width, LHuint tex_height, float angle, LHuint keep_ratio_flags, LHuint dst_mask, struct lh_fb2d_clip *dst_clip, LHuint src_mask, struct lh_fb2d_clip *src_clip, float global_alpha, LHbool is_visible)
{
	struct lh_core_ctx *core = window->core;
	struct lh_fb2d_surface *surface = NULL;
	struct lh_obj *obj;

	/* Blitting to NULL means blitting to window surface. */
	if(!parent_surface)
		parent_surface = core->fb2d->window_surface;

	surface = malloc(sizeof(*surface));
	if(!surface)
		goto failed_alloc_surface;
	obj = &surface->base;

	obj->custom_uniforms_data = malloc(sizeof(*obj->custom_uniforms_data) * FB2D_obj_class.shader_program.nb_custom_uniforms);
	if(!obj->custom_uniforms_data)
		goto failed_alloc_uniforms;

	obj->custom_textures = malloc(sizeof(*obj->custom_textures) * FB2D_obj_class.nb_textures);
	if(!obj->custom_textures)
		goto failed_alloc_textures;

	lh_vec3_zero(&obj->pos);
	lh_vec3_dup(&obj->angle, &LH_VEC3(0.f, 0.f, angle));
	obj->is_visible = LH_FALSE;
	obj->culling_enabled = LH_FALSE;
	obj->reversed_culling = LH_FALSE;
	obj->depth_test_enabled = LH_TRUE;

	surface->nancestors = parent_surface ? parent_surface->nancestors + 1 : 0;
	if(surface->nancestors) {
		surface->ancestors = malloc(sizeof(*surface->ancestors) * surface->nancestors);
		if(!surface->ancestors)
			goto failed_alloc_ancestors;
		surface->ancestors[0] = parent_surface;
		for(LHuint i = 0; i < parent_surface->nancestors; i++)
			surface->ancestors[i + 1] = parent_surface->ancestors[i];
	} else
		surface->ancestors = NULL;

	surface->is_visible = is_visible;
	surface->last_upd = 1;
	surface->last_ack = 0;
	surface->tex_width = tex_width;
	surface->tex_height = tex_height;
	surface->keep_ratio_flags = keep_ratio_flags;
	surface->dst_mask = dst_mask;
	surface->src_mask = src_mask;
	if(lhFB2DSurfaceSetDstClip(surface, dst_clip) < 0)
		goto failed_set_clip;
	if(lhFB2DSurfaceSetSrcClip(surface, src_clip) < 0)
		goto failed_set_clip;

	surface->upd_mask = LH_FB2D_UPD_VISIBLE_FLAG | LH_FB2D_UPD_Z_INDEX_FLAG;
	surface->dst_upd_mask = LH_FB2D_UPD_OFF_FLAG | LH_FB2D_UPD_POS_FLAG | LH_FB2D_UPD_SIZE_FLAG;
	surface->src_upd_mask = LH_FB2D_UPD_OFF_FLAG | LH_FB2D_UPD_POS_FLAG | LH_FB2D_UPD_SIZE_FLAG;
	surface->computed_width = 0;
	surface->computed_height = 0;
	surface->nchilds = 0;
	surface->childs = NULL;

	if(lhFB2DSurfaceSetGlobalAlpha(surface, global_alpha) < 0)
		goto failed_set_alpha;
	surface->z_index = parent_surface ? parent_surface->nchilds : 0;
	obj->custom_uniforms_data[UNIFORM_SAMPLER].as_int = 0; /* 1st sampler texture */

	obj->custom_textures[0] = texture;

	lhObjReg(window, &FB2D_obj_class, obj);

	if(parent_surface) {
		struct lh_fb2d_surface **childs;
		childs = realloc(parent_surface->childs, (parent_surface->nchilds + 1) * sizeof(*parent_surface->childs));
		if(!childs)
			goto failed_append_child;
		parent_surface->childs = childs;
		parent_surface->childs[parent_surface->nchilds] = surface;
		parent_surface->nchilds++;
		parent_surface->last_upd++;
	}
	return surface;

failed_append_child:
failed_set_alpha:
failed_set_clip:
	if(surface->ancestors)
		free(surface->ancestors);
failed_alloc_ancestors:
	free(obj->custom_textures);
failed_alloc_textures:
	free(obj->custom_uniforms_data);
failed_alloc_uniforms:
	free(surface);
failed_alloc_surface:
	return NULL;
}

int lhFB2DSurfaceSetRotation(struct lh_fb2d_surface *surface, float angle)
{
	struct lh_obj *obj = &surface->base;

	obj->angle.c.z = angle;
	surface->last_upd++;
	return 0;
}

int lhFB2DSurfaceSetKeepRatioFlags(struct lh_fb2d_surface *surface, LHuint keep_ratio_flags)
{
	surface->keep_ratio_flags = keep_ratio_flags;
	surface->dst_upd_mask |= LH_FB2D_UPD_OFF_FLAG | LH_FB2D_UPD_POS_FLAG | LH_FB2D_UPD_SIZE_FLAG;
	return 0;
}

int lhFB2DSurfaceSetTexture(struct lh_fb2d_surface *surface, LHuint texture, LHuint tex_width, LHuint tex_height)
{
	struct lh_obj *obj = &surface->base;

	surface->tex_width = tex_width;
	surface->tex_height = tex_height;
	obj->custom_textures[0] = texture;
	surface->last_upd++;
	return 0;
}

int lhFB2DSurfaceSetGlobalAlpha(struct lh_fb2d_surface *surface, float global_alpha)
{
	struct lh_obj *obj = &surface->base;

	if(global_alpha < 0.f || global_alpha > 1.f)
		return -1;
	obj->custom_uniforms_data[UNIFORM_GLOBAL_ALPHA].as_float = global_alpha;
	surface->last_upd++;
	return 0;
}

int lhFB2DSurfaceSetVisible(struct lh_fb2d_surface *surface, LHbool is_visible)
{
	surface->is_visible = is_visible;
	surface->upd_mask |= LH_FB2D_UPD_VISIBLE_FLAG;
	surface->last_upd++;
	return 0;
}

int lhFB2DSurfaceMoveUp(struct lh_fb2d_surface *surface)
{
	const struct lh_fb2d_surface *parent = surface->ancestors[0];
	struct lh_fb2d_surface *next_surf;

	if(surface->z_index >= parent->nchilds - 1)
		return 0;
	next_surf = parent->childs[surface->z_index + 1];
	surface->z_index++;
	next_surf->z_index--;
	parent->childs[surface->z_index] = surface;
	parent->childs[next_surf->z_index] = next_surf;
	surface->upd_mask |= LH_FB2D_UPD_Z_INDEX_FLAG;
	surface->last_upd++;
	next_surf->upd_mask |= LH_FB2D_UPD_Z_INDEX_FLAG;
	next_surf->last_upd++;
	return 0;
}

int lhFB2DSurfaceMoveDown(struct lh_fb2d_surface *surface)
{
	const struct lh_fb2d_surface *parent = surface->ancestors[0];
	struct lh_fb2d_surface *prev_surf;

	if(surface->z_index == 0)
		return 0;
	prev_surf = parent->childs[surface->z_index - 1];
	surface->z_index--;
	prev_surf->z_index++;
	parent->childs[surface->z_index] = surface;
	parent->childs[prev_surf->z_index] = prev_surf;
	surface->upd_mask |= LH_FB2D_UPD_Z_INDEX_FLAG;
	surface->last_upd++;
	prev_surf->upd_mask |= LH_FB2D_UPD_Z_INDEX_FLAG;
	prev_surf->last_upd++;
	return 0;
}

int lhFB2DSurfaceMoveAbove(struct lh_fb2d_surface *surface, struct lh_fb2d_surface *tgt_surf)
{
	const struct lh_fb2d_surface *parent = surface->ancestors[0];
	int dir;
	LHuint start, end;

	if(surface == tgt_surf)
		return 0;
	if(surface->ancestors[0] != tgt_surf->ancestors[0])
		return -1;
	if(tgt_surf->z_index < surface->z_index) {
		dir = 1;
		start = tgt_surf->z_index + 1;
		end = surface->z_index - 1;
	} else {
		dir = -1;
		start = surface->z_index + 1;
		end = tgt_surf->z_index;
	}
	for(LHuint i = start; i <= end; i++) {
		parent->childs[i]->z_index += dir;
		parent->childs[i + dir] = parent->childs[i];
		parent->childs[i + dir]->upd_mask |= LH_FB2D_UPD_Z_INDEX_FLAG;
		parent->childs[i + dir]->last_upd++;
	}
	surface->z_index = tgt_surf->z_index + 1;
	parent->childs[surface->z_index] = surface;
	surface->upd_mask |= LH_FB2D_UPD_Z_INDEX_FLAG;
	surface->last_upd++;
	return 0;
}

int lhFB2DSurfaceMoveBelow(struct lh_fb2d_surface *surface, struct lh_fb2d_surface *tgt_surf)
{
	const struct lh_fb2d_surface *parent = surface->ancestors[0];
	int dir;
	LHuint start, end;

	if(surface == tgt_surf)
		return 0;
	if(surface->ancestors[0] != tgt_surf->ancestors[0])
		return -1;
	if(tgt_surf->z_index < surface->z_index) {
		dir = 1;
		start = tgt_surf->z_index;
		end = surface->z_index - 1;
	} else {
		dir = -1;
		start = surface->z_index + 1;
		end = tgt_surf->z_index - 1;
	}
	for(LHuint i = start; i <= end; i++) {
		parent->childs[i]->z_index += dir;
		parent->childs[i + dir] = parent->childs[i];
		parent->childs[i + dir]->upd_mask |= LH_FB2D_UPD_Z_INDEX_FLAG;
		parent->childs[i + dir]->last_upd++;
	}
	surface->z_index = tgt_surf->z_index - 1;
	parent->childs[surface->z_index] = surface;
	surface->upd_mask |= LH_FB2D_UPD_Z_INDEX_FLAG;
	surface->last_upd++;
	return 0;
}

int lhFB2DSurfaceSwap(struct lh_fb2d_surface *surface, struct lh_fb2d_surface *other_surf)
{
	struct lh_fb2d_surface *parent = surface->ancestors[0];
	LHuint z_index;

	if(surface == other_surf)
		return 0;
	if(surface->ancestors[0] != other_surf->ancestors[0])
		return -1;
	z_index = surface->z_index;
	surface->z_index = other_surf->z_index;
	other_surf->z_index = z_index;
	parent->childs[surface->z_index] = surface;
	parent->childs[other_surf->z_index] = other_surf;
	surface->upd_mask |= LH_FB2D_UPD_Z_INDEX_FLAG;
	surface->last_upd++;
	other_surf->upd_mask |= LH_FB2D_UPD_Z_INDEX_FLAG;
	other_surf->last_upd++;
	return 0;
}

int lhFB2DSurfaceSetDstClipPctMode(struct lh_fb2d_surface *surface, LHuint mask)
{
	surface->dst_mask = mask;
	surface->dst_upd_mask |= LH_FB2D_UPD_OFF_FLAG | LH_FB2D_UPD_POS_FLAG | LH_FB2D_UPD_SIZE_FLAG;
	surface->last_upd++;
	return 0;
}

int lhFB2DSurfaceSetDstClip(struct lh_fb2d_surface *surface, struct lh_fb2d_clip *clip)
{
	if(clip->size.c.x < 0.f || clip->size.c.y < 0.f)
		return -1;
	lh_vec2_dup(&surface->dst_params.off, &clip->off);
	lh_vec2_dup(&surface->dst_params.pos, &clip->pos);
	lh_vec2_dup(&surface->dst_params.size, &clip->size);
	surface->dst_upd_mask |= LH_FB2D_UPD_OFF_FLAG | LH_FB2D_UPD_POS_FLAG | LH_FB2D_UPD_SIZE_FLAG;
	surface->last_upd++;
	return 0;
}

int lhFB2DSurfaceSetDstRectPctMode(struct lh_fb2d_surface *surface, LHuint mask)
{
	/* Reset old POS and SIZE flags */
	surface->dst_mask &= ~(LH_FB2D_UPD_POS_FLAG | LH_FB2D_UPD_SIZE_FLAG);
	/* Set new POS and SIZE flags */
	surface->dst_mask |= mask & (LH_FB2D_UPD_POS_FLAG | LH_FB2D_UPD_SIZE_FLAG);
	surface->dst_upd_mask |= LH_FB2D_UPD_POS_FLAG | LH_FB2D_UPD_SIZE_FLAG;
	surface->last_upd++;
	return 0;
}

int lhFB2DSurfaceSetDstRect(struct lh_fb2d_surface *surface, lh_vec4_t *rect)
{
	if(rect->c.z < 0.f || rect->c.w < 0.f)
		return -1;
	surface->dst_params.pos.c.x = rect->c.x;
	surface->dst_params.pos.c.y = rect->c.y;
	surface->dst_params.size.c.x = rect->c.z;
	surface->dst_params.size.c.y = rect->c.w;
	surface->dst_upd_mask |= LH_FB2D_UPD_POS_FLAG | LH_FB2D_UPD_SIZE_FLAG;
	surface->last_upd++;
	return 0;
}

int lhFB2DSurfaceSetDstOffPctMode(struct lh_fb2d_surface *surface, LHbool is_pct)
{
	if(is_pct)
		surface->dst_mask |= LH_FB2D_PCT_OFF_FLAG;
	else
		surface->dst_mask &= ~LH_FB2D_PCT_OFF_FLAG;
	surface->dst_upd_mask |= LH_FB2D_UPD_OFF_FLAG;
	surface->last_upd++;
	return 0;
}

int lhFB2DSurfaceSetDstOff(struct lh_fb2d_surface *surface, lh_vec2_t *off)
{
	lh_vec2_dup(&surface->dst_params.off, off);
	surface->dst_upd_mask |= LH_FB2D_UPD_OFF_FLAG;
	surface->last_upd++;
	return 0;
}

int lhFB2DSurfaceSetDstPosPctMode(struct lh_fb2d_surface *surface, LHbool is_pct)
{
	if(is_pct)
		surface->dst_mask |= LH_FB2D_PCT_POS_FLAG;
	else
		surface->dst_mask &= ~LH_FB2D_PCT_POS_FLAG;
	surface->dst_upd_mask |= LH_FB2D_UPD_POS_FLAG;
	surface->last_upd++;
	return 0;
}

int lhFB2DSurfaceSetDstPos(struct lh_fb2d_surface *surface, lh_vec2_t *pos)
{
	lh_vec2_dup(&surface->dst_params.pos, pos);
	surface->dst_upd_mask |= LH_FB2D_UPD_POS_FLAG;
	surface->last_upd++;
	return 0;
}

int lhFB2DSurfaceSetDstSizePctMode(struct lh_fb2d_surface *surface, LHbool is_pct)
{
	if(is_pct)
		surface->dst_mask |= LH_FB2D_PCT_SIZE_FLAG;
	else
		surface->dst_mask &= ~LH_FB2D_PCT_SIZE_FLAG;
	surface->dst_upd_mask |= LH_FB2D_UPD_SIZE_FLAG;
	surface->last_upd++;
	return 0;
}

int lhFB2DSurfaceSetDstSize(struct lh_fb2d_surface *surface, lh_vec2_t *size)
{
	if(size->c.x < 0.f || size->c.y < 0.f)
		return -1;
	lh_vec2_dup(&surface->dst_params.size, size);
	surface->dst_upd_mask |= LH_FB2D_UPD_SIZE_FLAG;
	surface->last_upd++;
	return 0;
}

int lhFB2DSurfaceSetSrcClipPctMode(struct lh_fb2d_surface *surface, LHuint mask)
{
	surface->src_mask = mask;
	surface->src_upd_mask |= LH_FB2D_UPD_OFF_FLAG | LH_FB2D_UPD_POS_FLAG | LH_FB2D_UPD_SIZE_FLAG;
	surface->last_upd++;
	return 0;
}

int lhFB2DSurfaceSetSrcClip(struct lh_fb2d_surface *surface, struct lh_fb2d_clip *clip)
{
	if(clip->size.c.x < 0.f || clip->size.c.y < 0.f)
		return -1;
	lh_vec2_dup(&surface->src_params.off, &clip->off);
	lh_vec2_dup(&surface->src_params.pos, &clip->pos);
	lh_vec2_dup(&surface->src_params.size, &clip->size);
	surface->src_upd_mask |= LH_FB2D_UPD_OFF_FLAG | LH_FB2D_UPD_POS_FLAG | LH_FB2D_UPD_SIZE_FLAG;
	surface->last_upd++;
	return 0;
}

int lhFB2DSurfaceSetSrcRectPctMode(struct lh_fb2d_surface *surface, LHuint mask)
{
	/* Reset old POS and SIZE flags */
	surface->src_mask &= ~(LH_FB2D_UPD_POS_FLAG | LH_FB2D_UPD_SIZE_FLAG);
	/* Set new POS and SIZE flags */
	surface->src_mask |= mask & (LH_FB2D_UPD_POS_FLAG | LH_FB2D_UPD_SIZE_FLAG);
	surface->src_upd_mask |= LH_FB2D_UPD_POS_FLAG | LH_FB2D_UPD_SIZE_FLAG;
	surface->last_upd++;
	return 0;
}

int lhFB2DSurfaceSetSrcRect(struct lh_fb2d_surface *surface, lh_vec4_t *rect)
{
	if(rect->c.z < 0.f || rect->c.w < 0.f)
		return -1;
	surface->src_params.pos.c.x = rect->c.x;
	surface->src_params.pos.c.y = rect->c.y;
	surface->src_params.size.c.x = rect->c.z;
	surface->src_params.size.c.y = rect->c.w;
	surface->src_upd_mask |= LH_FB2D_UPD_POS_FLAG | LH_FB2D_UPD_SIZE_FLAG;
	surface->last_upd++;
	return 0;
}

int lhFB2DSurfaceSetSrcOffPctMode(struct lh_fb2d_surface *surface, LHbool is_pct)
{
	if(is_pct)
		surface->src_mask |= LH_FB2D_PCT_OFF_FLAG;
	else
		surface->src_mask &= ~LH_FB2D_PCT_OFF_FLAG;
	surface->src_upd_mask |= LH_FB2D_UPD_OFF_FLAG;
	surface->last_upd++;
	return 0;
}

int lhFB2DSurfaceSetSrcOff(struct lh_fb2d_surface *surface, lh_vec2_t *off)
{
	lh_vec2_dup(&surface->src_params.off, off);
	surface->src_upd_mask |= LH_FB2D_UPD_OFF_FLAG;
	surface->last_upd++;
	return 0;
}

int lhFB2DSurfaceSetSrcPosPctMode(struct lh_fb2d_surface *surface, LHbool is_pct)
{
	if(is_pct)
		surface->src_mask |= LH_FB2D_PCT_POS_FLAG;
	else
		surface->src_mask &= ~LH_FB2D_PCT_POS_FLAG;
	surface->src_upd_mask |= LH_FB2D_UPD_POS_FLAG;
	surface->last_upd++;
	return 0;
}

int lhFB2DSurfaceSetSrcPos(struct lh_fb2d_surface *surface, lh_vec2_t *pos)
{
	lh_vec2_dup(&surface->src_params.pos, pos);
	surface->src_upd_mask |= LH_FB2D_UPD_POS_FLAG;
	surface->last_upd++;
	return 0;
}

int lhFB2DSurfaceSetSrcSizePctMode(struct lh_fb2d_surface *surface, LHbool is_pct)
{
	if(is_pct)
		surface->src_mask |= LH_FB2D_PCT_SIZE_FLAG;
	else
		surface->src_mask &= ~LH_FB2D_PCT_SIZE_FLAG;
	surface->src_upd_mask |= LH_FB2D_UPD_SIZE_FLAG;
	surface->last_upd++;
	return 0;
}

int lhFB2DSurfaceSetSrcSize(struct lh_fb2d_surface *surface, lh_vec2_t *size)
{
	if(size->c.x < 0.f || size->c.y < 0.f)
		return -1;
	lh_vec2_dup(&surface->src_params.size, size);
	surface->src_upd_mask |= LH_FB2D_UPD_SIZE_FLAG;
	surface->last_upd++;
	return 0;
}

static inline LHbool is_pos_in_surface(struct lh_fb2d_surface *surface, lh_vec2_t *pointed_pos) {
	const float surface_x = surface->base.custom_uniforms_data[UNIFORM_DST_OFF].as_vec2.a[0] + surface->base.custom_uniforms_data[UNIFORM_DST_RECT].as_vec4.a[0];
	const float surface_y = surface->base.custom_uniforms_data[UNIFORM_DST_OFF].as_vec2.a[1] + surface->base.custom_uniforms_data[UNIFORM_DST_RECT].as_vec4.a[1];
	const float surface_w = surface->base.custom_uniforms_data[UNIFORM_DST_RECT].as_vec4.a[2];
	const float surface_h = surface->base.custom_uniforms_data[UNIFORM_DST_RECT].as_vec4.a[3];

	return betweenf(pointed_pos->c.x, surface_x, surface_x + surface_w)
			&& betweenf(pointed_pos->c.y, surface_y, surface_y + surface_h);
}

struct lh_fb2d_surface *lhFB2DGetPointedSurface(struct lh_window *window) {
	struct lh_fb2d_ctx *fb2d = window->core->fb2d;
	lh_vec2_t *pointed_pos_pct;
	struct lh_fb2d_surface *current_surface;
	struct lh_fb2d_surface *next_surface;

	if(fb2d->pointed_surface)
		return fb2d->pointed_surface;

	pointed_pos_pct = &LH_VEC2((float)fb2d->pointed_pos.c.x / window->width, (float)fb2d->pointed_pos.c.y / window->height);
	current_surface = NULL;
	next_surface = fb2d->window_surface;

	do {
		LHuint i;
		current_surface = next_surface;
		next_surface = NULL;

		i = current_surface->nchilds;
		while(i-- != 0) {
			if(current_surface->childs[i]->is_visible && is_pos_in_surface(current_surface->childs[i], pointed_pos_pct)) {
				next_surface = current_surface->childs[i];
				break;
			}
		}
	} while (next_surface);

	return current_surface;
}

void lhFB2DRemoveSurface(struct lh_window *window, struct lh_fb2d_surface *surface)
{
	struct lh_obj *obj = &surface->base;
	struct lh_fb2d_surface *parent = !is_window_surface(window, surface) ? surface->ancestors[0] : NULL;

	/* Hide possible artifacts. */
	lhFB2DSurfaceSetVisible(surface, LH_FALSE);

	/*
	 * As the child at (surface->nchilds - 1) has the highest z_index, if we
	 * remove it, there will be no need for adjusting the z_indexes, and we'll
	 * save some precious CPU time.
	 */
	while(surface->nchilds)
		lhFB2DRemoveSurface(window, surface->childs[surface->nchilds - 1]);
	if(!is_window_surface(window, surface)) {
		/* Fix all the z_indexes. */
		lhFB2DSurfaceMoveAbove(surface, parent->childs[parent->nchilds - 1]);
		if(parent->nchilds - 1 > 0) {
			struct lh_fb2d_surface **childs;

			/*
			 * It's not a problem if realloc fails to shrink, in this case we'll
			 * just end up with too much memory...
			 */
			childs = realloc(parent->childs, sizeof(*parent->childs) * (parent->nchilds - 1));
			if(childs)
				parent->childs = childs;
		} else {
			free(parent->childs);
			parent->childs = NULL;
		}
		parent->nchilds--;
		/*
		 * We don't need to increment parent->last_upd, as the childs sequence
		 * does not change.
		 */
	}
	lhObjUnreg(window, obj);
	if(!is_window_surface(window, surface))
		free(surface->ancestors);
	free(obj->custom_textures);
	free(obj->custom_uniforms_data);
	free(surface);

	window->core->fb2d->pointed_surface = NULL;
}

void lhFB2DClear(struct lh_window *window)
{
	/*
	 * As the child at (surface->nchilds - 1) has the highest z_index, if we
	 * remove it, there will be no need for adjusting the z_indexes, and we'll
	 * save some precious CPU time.
	 */
	while(window->core->fb2d->window_surface->nchilds)
		lhFB2DRemoveSurface(window, window->core->fb2d->window_surface->childs[0]);
}
