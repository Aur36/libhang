/*
 * Copyright (C) 2015-2016 Guillaume Charifi
 * Based on great work from KdotJPG (https://gist.github.com/KdotJPG/b1270127455a94ac5d19)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 3 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <stdlib.h>
#include <string.h>
#include <math.h>

#include <libhang.h>

#define STRETCH_CONSTANT_1D  -0.292893218813452476 /* (1/sqrt(1+1)-1)/1 */
#define SQUISH_CONSTANT_1D    0.414213562373095049 /* (sqrt(1+1)-1)/1 */
#define STRETCH_CONSTANT_2D  -0.211324865405187118 /* (1/sqrt(2+1)-1)/2 */
#define SQUISH_CONSTANT_2D    0.366025403784438647 /* (sqrt(2+1)-1)/2 */
#define STRETCH_CONSTANT_3D (-1. / 6.)             /* (1/sqrt(3+1)-1)/3 */
#define SQUISH_CONSTANT_3D  ( 1. / 3.)             /* (sqrt(3+1)-1)/3 */
#define STRETCH_CONSTANT_4D  -0.138196601125010515 /* (1/sqrt(4+1)-1)/4 */
#define SQUISH_CONSTANT_4D    0.309016994374947424 /* (sqrt(4+1)-1)/4 */

#define NORM_CONSTANT_2D 47

struct lh_osimplex_noise {
	LH64u  seed;
	LHbyte rand_array[256];
};

struct lh_osimplex_noise *lh_osimplex_create(struct lh_osimplex_noise **posn)
{
	if(*posn != OSN_INVAL)
		return *posn;
	*posn = calloc(1, sizeof(**posn));
	if(!*posn)
		return OSN_INVAL;
	return *posn;
}

void lh_osimplex_destroy(struct lh_osimplex_noise **posn)
{
	struct lh_osimplex_noise *osn = *posn;
	if(osn == OSN_INVAL)
		return;
	free(osn);
	*posn = OSN_INVAL;
}

LH64u lh_osimplex_get_seed(struct lh_osimplex_noise *osn)
{
	return osn->seed;
}

void lh_osimplex_set_seed(struct lh_osimplex_noise *osn, LH64u seed)
{
	LHubyte source[256];

	osn->seed = seed;
	for(LHubyte i = 0;; i++) {
		source[i] = i;
		if(i == 255)
			break;
	}
	seed = seed * UINT64_C(6364136223846793005) + UINT64_C(1442695040888963407);
	seed = seed * UINT64_C(6364136223846793005) + UINT64_C(1442695040888963407);
	seed = seed * UINT64_C(6364136223846793005) + UINT64_C(1442695040888963407);
	for(LHubyte i = 255;; i--) {
		int r;
		seed = seed * UINT64_C(6364136223846793005) + UINT64_C(1442695040888963407);
		r = (int)((seed + 31) % (i + 1));
		if(r < 0)
			r += (i + 1);
		osn->rand_array[i] = source[r];
		source[r] = source[i];
		if(i == 0)
			break;
	}
}

static inline float extrapolate2D(struct lh_osimplex_noise *osn, int xsb, int ysb, float dx, float dy) {
	static const float gradients[] = {
		 5.,  2.,   2.,  5.,
		-5.,  2.,  -2.,  5.,
		 5., -2.,   2., -5.,
		-5., -2.,  -2., -5.
	};
	int idx = osn->rand_array[(osn->rand_array[xsb & 0xFF] + ysb) & 0xFF] & 0x0E;
	return gradients[idx] * dx
		+ gradients[idx + 1] * dy;
}

static inline float contrib2D(struct lh_osimplex_noise *osn, float xsb, float ysb, float dx0, float dy0, LHuint cx, LHuint cy, float in_val) {
	float dx1 = dx0 - cx - SQUISH_CONSTANT_2D;
	float dy1 = dy0 - cy - SQUISH_CONSTANT_2D;
	float attn = 2. - dx1 * dx1 - dy1 * dy1;
	if(attn > 0.) {
		attn *= attn;
		in_val += attn * attn * extrapolate2D(osn, xsb + cx, ysb + cy, dx1, dy1);
	}
	return in_val;
}

float lh_osimplex_noise2D(struct lh_osimplex_noise *osn, float x, float y)
{
	//Place input coordinates onto grid.
	float stretchOffset = (x + y) * STRETCH_CONSTANT_2D;
	float xs = x + stretchOffset;
	float ys = y + stretchOffset;

	//Floor to get grid coordinates of rhombus (stretched square) super-cell origin.
	int xsb = floor(xs);
	int ysb = floor(ys);

	//Skew out to get actual coordinates of rhombus origin. We'll need these later.
	float squishOffset = (xsb + ysb) * SQUISH_CONSTANT_2D;
	float xb = xsb + squishOffset;
	float yb = ysb + squishOffset;

	//Compute grid coordinates relative to rhombus origin.
	float xins = xs - xsb;
	float yins = ys - ysb;

	//Sum those together to get a value that determines which region we're in.
	float inSum = xins + yins;

	//Positions relative to origin point.
	float dx0 = x - xb;
	float dy0 = y - yb;

	//We'll be defining these inside the next block and using them afterwards.
	float dx_ext, dy_ext;
	int xsv_ext, ysv_ext;

	float value = 0;
	value = contrib2D(osn, xsb, ysb, dx0, dy0, 1, 0, value);
	value = contrib2D(osn, xsb, ysb, dx0, dy0, 0, 1, value);

	if (inSum <= 1.) { //We're inside the triangle (2-Simplex) at (0,0)
		float zins = 1. - inSum;
		if (zins > xins || zins > yins) { //(0,0) is one of the closest two triangular vertices
			if (xins > yins) {
				xsv_ext = xsb + 1.;
				ysv_ext = ysb - 1.;
				dx_ext  = dx0 - 1.;
				dy_ext  = dy0 + 1.;
			} else {
				xsv_ext = xsb - 1.;
				ysv_ext = ysb + 1.;
				dx_ext  = dx0 + 1.;
				dy_ext  = dy0 - 1.;
			}
		} else { //(1,0) and (0,1) are the closest two vertices.
			xsv_ext = xsb + 1.;
			ysv_ext = ysb + 1.;
			dx_ext = dx0 - 1. - 2. * SQUISH_CONSTANT_2D;
			dy_ext = dy0 - 1. - 2. * SQUISH_CONSTANT_2D;
		}
	} else { //We're inside the triangle (2-Simplex) at (1,1)
		float zins = 2. - inSum;
		if (zins < xins || zins < yins) { //(0,0) is one of the closest two triangular vertices
			if (xins > yins) {
				xsv_ext = xsb + 2.;
				ysv_ext = ysb + 0.;
				dx_ext = dx0 - 2. - 2. * SQUISH_CONSTANT_2D;
				dy_ext = dy0 + 0. - 2. * SQUISH_CONSTANT_2D;
			} else {
				xsv_ext = xsb + 0.;
				ysv_ext = ysb + 2.;
				dx_ext = dx0 + 0. - 2. * SQUISH_CONSTANT_2D;
				dy_ext = dy0 - 2. - 2. * SQUISH_CONSTANT_2D;
			}
		} else { //(1,0) and (0,1) are the closest two vertices.
			dx_ext = dx0;
			dy_ext = dy0;
			xsv_ext = xsb;
			ysv_ext = ysb;
		}
		xsb += 1.;
		ysb += 1.;
		dx0 = dx0 - 1. - 2. * SQUISH_CONSTANT_2D;
		dy0 = dy0 - 1. - 2. * SQUISH_CONSTANT_2D;
	}

	//Contribution (0,0) or (1,1)
	float attn0 = 2 - dx0 * dx0 - dy0 * dy0;
	if (attn0 > 0) {
		attn0 *= attn0;
		value += attn0 * attn0 * extrapolate2D(osn, xsb, ysb, dx0, dy0);
	}

	//Extra Vertex
	float attn_ext = 2 - dx_ext * dx_ext - dy_ext * dy_ext;
	if (attn_ext > 0) {
		attn_ext *= attn_ext;
		value += attn_ext * attn_ext * extrapolate2D(osn, xsv_ext, ysv_ext, dx_ext, dy_ext);
	}

	return value / NORM_CONSTANT_2D;
}
