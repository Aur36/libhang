/*
 * Copyright (C) 2014-2016 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 3 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <stdio.h>
#include <stdlib.h>

#include <malloc.h> /* _aligned_malloc() */
#include <windows.h>

#include <libhang.h>
#include "driver_iface.h"
#include "windows-private.h"

#ifndef InterlockedExchangeAcquire
#  define InterlockedExchangeAcquire InterlockedExchange
#endif
#if defined __MINGW32__ || defined __MINGW64__
#  ifndef InterlockedAddNoFence
#    define InterlockedAddNoFence InterlockedAdd
#  endif
#endif /* __MINGW32__ || __MINGW64__ */

static struct lh_sys sys;

static int ILHOSDriver_Init(void)
{
	sys.perf_counter_freq.QuadPart = 0;
	QueryPerformanceFrequency(&sys.perf_counter_freq);
	if(!sys.perf_counter_freq.QuadPart)
		sys.perf_counter_freq.QuadPart = 1;
	return 0;
}

static int ILHOSDriver_PreDeinit(void)
{
	return 0;
}

static int ILHOSDriver_Deinit(void)
{
	return 0;
}

static void *ILHOSDriver_MallocAligned(size_t alignment, size_t size)
{
	return _aligned_malloc(size, alignment);
}

static void ILHOSDriver_Free(void *ptr)
{
	_aligned_free(ptr);
}

static void ILHOSDriver_Delay(LHuint msec)
{
	Sleep(msec);
}

static void ILHOSDriver_DelayUSec(LHuint usec)
{
	LARGE_INTEGER init_pc, pc;

	if(!sys.perf_counter_freq.QuadPart)
		return;
	init_pc.QuadPart = 0;
	QueryPerformanceCounter(&init_pc);
	if(!init_pc.QuadPart)
		return;
	do {
		pc.QuadPart = 0;
		QueryPerformanceCounter(&pc);
	} while(pc.QuadPart && ((pc.QuadPart - init_pc.QuadPart) * 1000000) / sys.perf_counter_freq.QuadPart < usec);
}

static long ILHOSDriver_AtomicCounterGet(struct lh_atomic_counter *lh_cnt)
{
	union lh_atomic_counter_blob *cnt = LH_CONTAINER_OF(lh_cnt, union lh_atomic_counter_blob, base);
	const uintptr_t uptr = (uintptr_t)&cnt->scratch;
	LONG *ptr;
	const uintptr_t pot = lh_next_pot_u32(sizeof(*ptr));

	ptr = (void *)((uptr & ~(pot - 1)) + pot);
	return InterlockedAddNoFence(ptr, 0);
}

static long ILHOSDriver_AtomicCounterSet(struct lh_atomic_counter *lh_cnt, long val)
{
	union lh_atomic_counter_blob *cnt = LH_CONTAINER_OF(lh_cnt, union lh_atomic_counter_blob, base);
	uintptr_t uptr = (uintptr_t)&cnt->scratch;
	LONG *ptr;
	const uint32_t pot = lh_next_pot_u32(sizeof(*ptr));

	ptr = (void *)((uptr & ~(uintptr_t)(pot - 1)) + pot);
	InterlockedExchangeAcquire(ptr, val);
	return val;
}

static long ILHOSDriver_AtomicCounterInc(struct lh_atomic_counter *lh_cnt)
{
	union lh_atomic_counter_blob *cnt = LH_CONTAINER_OF(lh_cnt, union lh_atomic_counter_blob, base);
	uintptr_t uptr = (uintptr_t)&cnt->scratch;
	LONG *ptr;
	const uint32_t pot = lh_next_pot_u32(sizeof(*ptr));

	ptr = (void *)((uptr & ~(uintptr_t)(pot - 1)) + pot);
	return InterlockedIncrement(ptr);
}

static long ILHOSDriver_AtomicCounterDec(struct lh_atomic_counter *lh_cnt)
{
	union lh_atomic_counter_blob *cnt = LH_CONTAINER_OF(lh_cnt, union lh_atomic_counter_blob, base);
	uintptr_t uptr = (uintptr_t)&cnt->scratch;
	LONG *ptr;
	const uint32_t pot = lh_next_pot_u32(sizeof(*ptr));

	ptr = (void *)((uptr & ~(uintptr_t)(pot - 1)) + pot);
	return InterlockedDecrementRelease(ptr);
}

static DWORD WINAPI thread_wrap(LPVOID params)
{
	struct lh_thread_blob *thread = params;
	int ret = 0;

	ret = thread->func(thread->params);

	thread->is_running = LH_FALSE;
	return (DWORD)ret;
}

static struct lh_thread *ILHOSDriver_ThreadCreate(int (*func)(void *), void *params)
{
	struct lh_thread_blob *thread;

	thread = malloc(sizeof(*thread));
	if(!thread)
		return NULL;

	thread->func = func;
	thread->params = params;

	thread->is_launched = LH_TRUE;
	thread->is_running = LH_TRUE;

	thread->handle = CreateThread(NULL, 0, thread_wrap, thread, 0, NULL);
	if(!thread->handle) {
		free(thread);
		return NULL;
	}
	return &thread->base;
}

static int ILHOSDriver_ThreadWait(struct lh_thread *lh_thread, int *status)
{
	struct lh_thread_blob *thread = (struct lh_thread_blob *)lh_thread;
	DWORD thread_status;
	int ret;

	if(!thread->is_launched)
		return 0;

	ret = WaitForSingleObject(thread->handle, INFINITE);
	if(ret != WAIT_OBJECT_0)
		return -1;
	ret = GetExitCodeThread(thread->handle, &thread_status);
	if(!ret && status)
		return -1;
	if(status)
		*status = (int)thread_status;

	thread->is_launched = LH_FALSE;
	return 0;
}

static LHbool ILHOSDriver_ThreadIsRunning(struct lh_thread *lh_thread)
{
	struct lh_thread_blob *thread = (struct lh_thread_blob *)lh_thread;
	return thread->is_running;
}

static int ILHOSDriver_ThreadRelease(struct lh_thread *lh_thread)
{
	struct lh_thread_blob *thread = (struct lh_thread_blob *)lh_thread;
	int ret;

	if(ILHOSDriver_ThreadIsRunning(lh_thread))
		return -1;

	ret = ILHOSDriver_ThreadWait(lh_thread, NULL);
	if(ret < 0)
		return ret;

	free(thread);
	return 0;
}

static int ILHOSDriver_ThreadDestroy(struct lh_thread *lh_thread)
{
	struct lh_thread_blob *thread = (struct lh_thread_blob *)lh_thread;
	int ret;

	ret = TerminateThread(thread->handle, 0);
	if(!ret)
		return -1;

	free(lh_thread);
	return 0;
}

static struct lh_spinlock *ILHOSDriver_SpinCreate(void)
{
	struct lh_spinlock_blob *spinlock;

	spinlock = malloc(sizeof(*spinlock));
	if(!spinlock)
		return NULL;

	spinlock->lock = ILHOSDriver_MallocAligned(lh_next_pot_u32(sizeof(*spinlock->lock)), sizeof(*spinlock->lock));
	if(!spinlock->lock) {
		free(spinlock);
		return NULL;
	}
	*spinlock->lock = 0;

	return &spinlock->base;
}

static int ILHOSDriver_SpinLock(struct lh_spinlock *base_spinlock)
{
	struct lh_spinlock_blob *spinlock = LH_CONTAINER_OF(base_spinlock, struct lh_spinlock_blob, base);

	while(InterlockedCompareExchange(spinlock->lock, -1, 0) != 0)
		;
	return 0;
}

static int ILHOSDriver_SpinUnlock(struct lh_spinlock *base_spinlock)
{
	struct lh_spinlock_blob *spinlock = LH_CONTAINER_OF(base_spinlock, struct lh_spinlock_blob, base);

	InterlockedExchange(spinlock->lock, 0);
	return 0;
}

static int ILHOSDriver_SpinDestroy(struct lh_spinlock *base_spinlock)
{
	struct lh_spinlock_blob *spinlock = LH_CONTAINER_OF(base_spinlock, struct lh_spinlock_blob, base);

	ILHOSDriver_Free(spinlock->lock);
	free(spinlock);
	return 0;
}

static struct lh_mutex *ILHOSDriver_MutexCreate(void)
{
	struct lh_mutex_blob *mutex;

	mutex = malloc(sizeof(*mutex));
	if(!mutex)
		return NULL;

	/* Auto-reset, initially signaled */
	mutex->lock = CreateEventA(NULL, FALSE, TRUE, NULL);
	mutex->thread_id = (DWORD)-1;
	mutex->count = 0;

	return &mutex->base;
}

static int ILHOSDriver_MutexLock(struct lh_mutex *lh_mutex)
{
	struct lh_mutex_blob *mutex = (struct lh_mutex_blob *)lh_mutex;
	DWORD cur_thread_id;

	cur_thread_id = GetCurrentThreadId();
	if(mutex->thread_id != cur_thread_id) {
		WaitForSingleObject(mutex->lock, INFINITE);
		mutex->thread_id = cur_thread_id;
	}
	mutex->count++;

	return 0;
}

static int ILHOSDriver_MutexUnlock(struct lh_mutex *lh_mutex)
{
	struct lh_mutex_blob *mutex = (struct lh_mutex_blob *)lh_mutex;

	if(mutex->thread_id != GetCurrentThreadId()
			|| !mutex->count)
		return -1;
	if(mutex->count-- == 1) {
		mutex->thread_id = (DWORD)-1;
		SetEvent(mutex->lock);
	}

	return 0;
}

static int ILHOSDriver_MutexDestroy(struct lh_mutex *lh_mutex)
{
	struct lh_mutex_blob *mutex = (struct lh_mutex_blob *)lh_mutex;

	CloseHandle(mutex->lock);
	free(mutex);

	return 0;
}

static struct lh_cond *ILHOSDriver_CondCreate(void)
{
	struct lh_cond_blob *cond;

	cond = malloc(sizeof(*cond));
	if(!cond)
		return NULL;

	/* Auto-reset, initially signaled */
	cond->count_lock = CreateEventA(NULL, FALSE, TRUE, NULL);
	if(!cond->count_lock)
		goto failed_count_lock;

	/* Auto-reset, initially non signaled */
	cond->awoken_evt = CreateEventA(NULL, FALSE, FALSE, NULL);
	if(!cond->awoken_evt)
		goto failed_awoken_evt;

	/* Auto-reset, initially non signaled */
	cond->evt = CreateEventA(NULL, FALSE, FALSE, NULL);
	if(!cond->evt)
		goto failed_evt;

	cond->count = 0;
	return &cond->base;

failed_evt:
	CloseHandle(cond->awoken_evt);
failed_awoken_evt:
	CloseHandle(cond->count_lock);
failed_count_lock:
	free(cond);
	return NULL;
}

static int ILHOSDriver_CondWait(struct lh_cond *lh_cond)
{
	struct lh_cond_blob *cond = (struct lh_cond_blob *)lh_cond;
	DWORD ret;

	ret = WaitForSingleObject(cond->count_lock, INFINITE);
	if(ret != WAIT_OBJECT_0)
		return -1;
	cond->count++;
	SignalObjectAndWait(cond->count_lock, cond->evt, INFINITE, FALSE);
	if(ret != WAIT_OBJECT_0) {
		cond->count--;
		SetEvent(cond->count_lock);
		return -1;
	}
	SetEvent(cond->awoken_evt);
	return 0;
}

static int ILHOSDriver_CondWakeSingle(struct lh_cond *lh_cond)
{
	struct lh_cond_blob *cond = (struct lh_cond_blob *)lh_cond;
	DWORD ret;

	ret = WaitForSingleObject(cond->count_lock, INFINITE);
	if(ret != WAIT_OBJECT_0)
		return -1;
	if(!cond->count)
		return 0;
	SetEvent(cond->evt);
	cond->count--;
	ret = WaitForSingleObject(cond->awoken_evt, INFINITE);
	if(ret != WAIT_OBJECT_0) {
		SetEvent(cond->count_lock);
		return -1;
	}
	SetEvent(cond->count_lock);
	return 0;
}

static int ILHOSDriver_CondWakeAll(struct lh_cond *lh_cond)
{
	struct lh_cond_blob *cond = (struct lh_cond_blob *)lh_cond;
	DWORD ret;

	ret = WaitForSingleObject(cond->count_lock, INFINITE);
	if(ret != WAIT_OBJECT_0)
		return -1;
	while(cond->count > 0) {
		SetEvent(cond->evt);
		cond->count--;
		ret = WaitForSingleObject(cond->awoken_evt, INFINITE);
		if(ret != WAIT_OBJECT_0)
			break;
	}
	SetEvent(cond->count_lock);
	return 0;
}

static int ILHOSDriver_CondDestroy(struct lh_cond *lh_cond)
{
	struct lh_cond_blob *cond = (struct lh_cond_blob *)lh_cond;

	CloseHandle(cond->count_lock);
	CloseHandle(cond->awoken_evt);
	CloseHandle(cond->evt);
	free(cond);
	return 0;
}

ILHOSDriver __lhOSDriver_windows = {
	.base = {
		.Name      = "windows",
		.Init      = ILHOSDriver_Init,
		.PreDeinit = ILHOSDriver_PreDeinit,
		.Deinit    = ILHOSDriver_Deinit
	},
	.MallocAligned    = ILHOSDriver_MallocAligned,
	.Free             = ILHOSDriver_Free,
	.Delay            = ILHOSDriver_Delay,
	.DelayUSec        = ILHOSDriver_DelayUSec,
	.AtomicCounterGet = ILHOSDriver_AtomicCounterGet,
	.AtomicCounterSet = ILHOSDriver_AtomicCounterSet,
	.AtomicCounterInc = ILHOSDriver_AtomicCounterInc,
	.AtomicCounterDec = ILHOSDriver_AtomicCounterDec,
	.ThreadCreate     = ILHOSDriver_ThreadCreate,
	.ThreadWait       = ILHOSDriver_ThreadWait,
	.ThreadIsRunning  = ILHOSDriver_ThreadIsRunning,
	.ThreadRelease    = ILHOSDriver_ThreadRelease,
	.ThreadDestroy    = ILHOSDriver_ThreadDestroy,
	.SpinCreate       = ILHOSDriver_SpinCreate,
	.SpinLock         = ILHOSDriver_SpinLock,
	.SpinUnlock       = ILHOSDriver_SpinUnlock,
	.SpinDestroy      = ILHOSDriver_SpinDestroy,
	.MutexCreate      = ILHOSDriver_MutexCreate,
	.MutexLock        = ILHOSDriver_MutexLock,
	.MutexUnlock      = ILHOSDriver_MutexUnlock,
	.MutexDestroy     = ILHOSDriver_MutexDestroy,
	.CondCreate       = ILHOSDriver_CondCreate,
	.CondWait         = ILHOSDriver_CondWait,
	.CondWakeSingle   = ILHOSDriver_CondWakeSingle,
	.CondWakeAll      = ILHOSDriver_CondWakeAll,
	.CondDestroy      = ILHOSDriver_CondDestroy
};
