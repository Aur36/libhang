/*
 * Copyright (C) 2014-2016 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 3 of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <stdlib.h>
#include <string.h>
#include <math.h>

#include <libhang.h>

struct lh_perlin_noise {
	int *rand_array;
	LHuint rand_array_size;
};

struct lh_perlin_noise *lh_pn_init(struct lh_perlin_noise **ppn, LHuint rand_array_size, const int *rand_array)
{
	struct lh_perlin_noise *pn;
	if(*ppn != PN_INVAL)
		return *ppn;
	pn = (struct lh_perlin_noise *)calloc(1, sizeof(*pn));
	if(!pn)
		return PN_INVAL;
	pn->rand_array_size = rand_array_size;
	pn->rand_array = (int *)calloc(rand_array_size * 2, sizeof(pn->rand_array[0]));
	if(!pn->rand_array) {
		free(pn);
		return PN_INVAL;
	}

	if(rand_array) {
		for(LHuint i = 0; i < rand_array_size; i++)
			pn->rand_array[i] = pn->rand_array[i + rand_array_size] = maxi(rand_array[i], rand_array_size);
	} else {
		for(LHuint i = 0; i < rand_array_size; i++)
			pn->rand_array[i] = pn->rand_array[i + rand_array_size] = (int)floor(((float)rand() / RAND_MAX) * rand_array_size);
	}
	*ppn = pn;
	return *ppn;
}

void lh_pn_end(struct lh_perlin_noise **ppn)
{
	struct lh_perlin_noise *pn = *ppn;
	if(pn == PN_INVAL)
		return;
	free(pn->rand_array);
	free(pn);
	*ppn = PN_INVAL;
}

unsigned int lh_pn_get_rand_array(struct lh_perlin_noise *pn, float *rand_array)
{
	if(rand_array)
		memcpy(rand_array, pn->rand_array, pn->rand_array_size * sizeof(*pn->rand_array));
	return pn->rand_array_size;
}

static inline float PN_lerp(float t, float a, float b) {
	return a+t*(b-a);
}

static inline float PN_fade(float t) {
	return t*t*t*(t*(t*6.0-15.0)+10.0);
}

static inline float PN_grad1D(int hash, float x) {
	return (hash & 1) == 0 ? x : -x;
}

static inline float PN_grad2D(int hash, float x, float y) {
	int h = hash & 3;
	float u = (h & 2) == 0 ? x : -x;
	float v = (h & 1) == 0 ? y : -y;
	return u + v;
}

static inline float PN_grad3D(int hash, float x, float y, float z) {
	int h = hash & 15;
	float u = h < 8 ? x : y;
	float v = h < 4 ? y : h == 12 || h == 14 ? x : z;
	return ((h & 1) == 0 ? u : -u) + ((h & 2) == 0 ? v : -v);
}

float lh_pn_noise1D(struct lh_perlin_noise *pn, float x)
{
	// Compute the cell coordinates
	int X = ((int)floor(x)) % pn->rand_array_size;

	// Retrieve the decimal part of the cell
	x -= floor(x);

	float u = PN_fade(x);

	return PN_lerp(u, PN_grad1D(pn->rand_array[X], x), PN_grad1D(pn->rand_array[X+1], x - 1));
}

float lh_pn_noise2D(struct lh_perlin_noise *pn, float x, float y)
{
	// Compute the cell coordinates
	int X = ((int)floor(x)) % pn->rand_array_size;
	int Y = ((int)floor(y)) % pn->rand_array_size;

	// Retrieve the decimal part of the cell
	x -= floor(x);
	y -= floor(y);

	// Smooth the curve
	float u = PN_fade(x);
	float v = PN_fade(y);

	// Fetch some randoms values from the table
	int A = pn->rand_array[X] + Y;
	int B = pn->rand_array[X + 1] + Y;

	// Interpolate between directions
	return PN_lerp(v, PN_lerp(u, PN_grad2D(pn->rand_array[A], x, y),
			PN_grad2D(pn->rand_array[B], x - 1, y)),
		PN_lerp(u, PN_grad2D(pn->rand_array[A + 1], x, y - 1),
			PN_grad2D(pn->rand_array[B + 1], x - 1, y - 1)));
}

float lh_pn_noise3D(struct lh_perlin_noise *pn, float x, float y, float z)
{
	// Compute the cell coordinates
	int X = ((int)floor(x)) % pn->rand_array_size;
	int Y = ((int)floor(y)) % pn->rand_array_size;
	int Z = ((int)floor(z)) % pn->rand_array_size;

	// Retrieve the decimal part of the cell
	x -= floor(x);
	y -= floor(y);
	z -= floor(z);

	// Smooth the curve
	float u = PN_fade(x);
	float v = PN_fade(y);
	float w = PN_fade(z);

	// Fetch some random values from the table
	int A = pn->rand_array[X] + Y;
	int AA = pn->rand_array[A] + Z;
	int AB = pn->rand_array[A + 1] + Z;
	int B = pn->rand_array[X + 1] + Y;
	int BA = pn->rand_array[B] + Z;
	int BB = pn->rand_array[B + 1] + Z;

	// Interpolate between directions
	return PN_lerp(w, PN_lerp(v, PN_lerp(u, PN_grad3D(pn->rand_array[AA], x, y, z),
				PN_grad3D(pn->rand_array[BA], x - 1, y, z)),
			PN_lerp(u, PN_grad3D(pn->rand_array[AB], x, y - 1, z),
				PN_grad3D(pn->rand_array[BB], x - 1, y - 1, z))),
		PN_lerp(v, PN_lerp(u, PN_grad3D(pn->rand_array[AA + 1], x, y, z - 1),
				PN_grad3D(pn->rand_array[BA + 1], x - 1, y, z - 1)),
			PN_lerp(u, PN_grad3D(pn->rand_array[AB + 1], x, y - 1, z - 1),
				PN_grad3D(pn->rand_array[BB + 1], x - 1, y - 1, z - 1))));
}
